using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPack : MonoBehaviour
{

    public MeshRenderer mesh;
    public Collider col;

    public void respawn()
    {
        mesh.enabled = true;
        col.enabled = true;
    }
    private void OnTriggerEnter(Collider other) {
        if (other.TryGetComponent<Damageable>(out Damageable damageable))
        {
           if (damageable.CurrentHealth < damageable.MaximumHealth)
           {
               Debug.Log("Health restored!");
               damageable.RestoreHealth();

                mesh.enabled = false;
                col.enabled = false;
                Invoke("respawn", 5f);
            }
        }
    }
}
