﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public enum WeaponShootType
{
    Manual,
    Automatic,
    Charge
}
public enum AbilitySlot
{
    Main,
    Alternative,
    Special
}

[System.Serializable]
public struct CrosshairData
{
    [Tooltip("The image that will be used for this weapon's crosshair")]
    public Sprite crosshairSprite;
    [Tooltip("The size of the crosshair image")]
    public int crosshairSize;
    [Tooltip("The default color of the crosshair image")]
    public Color crosshairDefaultColor;
    [Tooltip("The color of the crosshair image when pointing at something")]
    public Color crosshairTargetColor;
    [Tooltip("Crosshair hitable layer")]
    public LayerMask crossHairLayermask;
    [Tooltip("The image that will be used for this weapon's target lock crosshair")]
    public Sprite crosshairTargetLockSprite;
    [Tooltip("The size of the target lock crosshair image")]
    public int crosshairTargetLockSize;
}

public class WeaponController : NetworkBehaviour
{
    public AudioSync audiosync;
    public AudioSource gunSound;

    #region BaseVariables

    [Header("Information")]
    [Tooltip("The name that will be displayed in the UI for this weapon")]
    public string weaponName;
    [Tooltip("The image that will be displayed in the UI for this weapon")]
    public Sprite weaponIcon;
    // [Tooltip("Is the weapon Melee?")]
    // public bool isMelee;
    public GameObject spawnedVFX;


    [Tooltip("Default data for the crosshair")]
    public CrosshairData crosshairDataDefault;

    [Header("Internal References")]
    [Tooltip("The root object for the weapon, this is what will be deactivated when the weapon isn't active")]
    public GameObject weaponRoot;
    [Tooltip("Tip of the weapon, where the projectiles are shot")]
    public Transform weaponMuzzle;

    public Transform aimTransform;
    #endregion


    #region MainAbilityVariables

    [Header("Shoot Parameters")]
    [Tooltip("The type of weapon will affect how it shoots")]
    public WeaponShootType shootType;
    [Tooltip("The Attack prefab")]
    public AttackBase attackPrefab;
    [Tooltip("Minimum duration between two shots")]
    public float delayBetweenShots = 0.5f;
    [Tooltip("Angle for the cone in which the bullets will be shot randomly (0 means no spread at all)")]
    public float bulletSpreadAngle = 0f;
    public float spreadMultiplier = 0f;
    [Tooltip("Amount of bullets per shot")]
    public int bulletsPerShot = 1;
    // [Tooltip("Draw Trajectory")]
    // public bool shouldDrawTrajectory;

    [Header("Ammo Parameters")]
    [Tooltip("Amount of ammo reloaded per second")]
    public float ammoReloadRate = 1f;
    [Tooltip("Delay after the last shot before starting to reload")]
    public float ammoReloadDelay = 2f;
    [Tooltip("Maximum amount of ammo in the gun")]
    public float maxAmmo = 8;

    [Header("Charging parameters (charging weapons only)")]
    [Tooltip("Duration to reach maximum charge")]
    public float maxChargeDuration = 2f;
    [Tooltip("Initial ammo used when starting to charge")]
    public float ammoUsedOnStartCharge = 1f;
    [Tooltip("Additional ammo used when charge reaches its maximum")]
    public float ammoUsageRateWhileCharging = 1f;

    [Header("Object Limiting")]
    public List<GameObject> mainObjectsSpawned;
    public int mainObjectLimit;

    [Header("HUD UI")]
    [Tooltip("The sprite for the main attack UI")]
    public Sprite weaponUISprite;



    public Vector3 mainAbilityIconScale;
    public GameObject mainVFXPrefab;

    #endregion


    #region Don'tTouch

    public GameObject owner;
    public GameObject sourcePrefab;
    public ECM.Controllers.BaseCharacterController playerMovement;
    public PlayerAim playerAim;


    #endregion


    /// <summary>
    /// Private variables
    /// </summary>
    #region PrivateVariables

    float currentAmmo;
    float lastTimeShot = Mathf.NegativeInfinity;
    float timeBeginCharge;
    float currentAltAmmo;
    float lastTimeAltShot = Mathf.NegativeInfinity;
    float timeBeginAltCharge;
    float currentSpecAmmo;
    float lastTimeSpecShot = Mathf.NegativeInfinity;
    float timeBeginSpecCharge;


    float cameraSwitchTime;
    Vector3 lastMuzzlePosition;

    public bool isWeaponActive { get; private set; }
    public bool isCharging { get; private set; }
    public float currentAmmoRatio { get; private set; }
    public bool isCooling { get; private set; }
    public float currentCharge { get; private set; }
    public bool isAltCharging { get; private set; }
    public float currentAltAmmoRatio { get; private set; }
    public bool isAltCooling { get; private set; }
    public float currentAltCharge { get; private set; }
    public bool isSpecCharging { get; private set; }
    public float currentSpecAmmoRatio { get; private set; }
    public bool isSpecCooling { get; private set; }
    public float currentSpecCharge { get; private set; }
    #endregion



    private void Awake()
    {
       // playerMovement.muzzle.transform.position = weaponMuzzle.transform.position;
    }
    private void Start()
    {
        Invoke("InitiateSound", 1.0f);
        gunSound = GetComponent<AudioSource>();
    }
    void Update()
    {
        if (playerMovement != null)
        {
            UpdateAmmo();
            UpdateCharge();
        }
    }
    

    private void InitiateSound()
    {
        Debug.Log("I instantiate the gun Audio");
        audiosync = GameObject.FindGameObjectWithTag("SoundEffectControl").GetComponent<AudioSync>();
    }

    public void shootSound()
    {
        gunSound.Play();


    }


    public override void OnStartAuthority()
    {
        base.OnStartAuthority();

    }

    //Object limiting
    #region Object Limiting

    public void LimitObjects(GameObject go,AbilitySlot _abilitySlot)
    {
       
        switch (_abilitySlot)
        {
            case AbilitySlot.Main:
                if (mainObjectLimit > 0)
                {

                    if (mainObjectsSpawned.Count + 1 >= mainObjectLimit)
                    {
                        Destroy(mainObjectsSpawned[0]);
                        mainObjectsSpawned.RemoveAt(0);
                        mainObjectsSpawned.Add(go);
                    }
                    else
                    {
                        mainObjectsSpawned.Add(go);
                    }
                }
                break;
            default:
                break;
        }


    }
    public void AddObjectToAltList(GameObject go)
    {
        //objectsSpawned.RemoveAll(x => x == null);
        //if (objectsSpawned.Count +1 <= altobjectLimit)
        //{
        //    objectsSpawned.e(go);

        //}
        //else
        //{

        //}
    }
    public void AddObjectToSpecList(GameObject go)
    {
        //objectsSpawned.RemoveAll(x => x == null);
        //if (objectsSpawned.Count +1 <= altobjectLimit)
        //{
        //    objectsSpawned.e(go);

        //}
        //else
        //{

        //}
    }
    #endregion
    //Ammo
    #region Ammo

    void UpdateAmmo()
    {
        //Main
        if (lastTimeShot + ammoReloadDelay < Time.time && currentAmmo < maxAmmo && !isCharging)
        {
            // reloads weapon over time
            currentAmmo += ammoReloadRate * Time.deltaTime;

            // limits ammo to max value
            currentAmmo = Mathf.Clamp(currentAmmo, 0, maxAmmo);

            isCooling = true;
        }
        else
        {
            isCooling = false;
        }

        if (maxAmmo == Mathf.Infinity)
        {
            currentAmmoRatio = 1f;
        }
        else
        {
            currentAmmoRatio = currentAmmo / maxAmmo;
        }
    }

    void UpdateCharge()
    {
        //Main
        if (isCharging)
        {
            if (currentCharge < 1f)
            {
                float chargeLeft = 1f - currentCharge;

                // Calculate how much charge ratio to add this frame
                float chargeAdded = 0f;
                if (maxChargeDuration <= 0f)
                {
                    chargeAdded = chargeLeft;
                }
                chargeAdded = (1f / maxChargeDuration) * Time.deltaTime;
                chargeAdded = Mathf.Clamp(chargeAdded, 0f, chargeLeft);

                // See if we can actually add this charge
                float ammoThisChargeWouldRequire = chargeAdded * ammoUsageRateWhileCharging;
                //if (ammoThisChargeWouldRequire <= m_CurrentAmmo)
                {
                    // Use ammo based on charge added
                    UseAmmo(ammoThisChargeWouldRequire);

                    // set current charge ratio
                    currentCharge = Mathf.Clamp01(currentCharge + chargeAdded);
                }
            }
        }
    }

    public void UseAmmo(float _amount)
    {
        currentAmmo = Mathf.Clamp(currentAmmo - _amount, 0f, maxAmmo);
        lastTimeShot = Time.time;
    }
    #endregion
    //Inputs
    #region Inputs

    public bool HandleShootInputs(bool _inputDown, bool _inputHeld, bool _inputUp)
    {
        if (_inputUp && spawnedVFX != null)
        {
            RemoveVFX();
        }
        switch (shootType)
        {
            case WeaponShootType.Manual:
                if (_inputDown)
                {
                    return TryShoot();
                }
                return false;

            case WeaponShootType.Automatic:
                if (_inputHeld)
                {
                    return TryShoot();
                }
                return false;

            case WeaponShootType.Charge:
                if (_inputHeld)
                {
                    TryBeginCharge();
                }
                if (_inputUp)
                {
                    return TryReleaseCharge();
                }
                return false;

            default:
                return false;
        }

    }

    #endregion

    // Main
    #region MainShoot

    bool TryShoot()
    {
        Debug.Log("Trying to Shoot");
        if (currentAmmo >= 1f && lastTimeShot + delayBetweenShots < Time.time)
        {
            audiosync.PlaySound(0, this.transform.position);
            HandleShoot(currentCharge);
            currentAmmo -= 1;
            return true;
        }

        return false;
    }

    bool TryBeginCharge()
    {
        if (!isCharging && currentAmmo >= ammoUsedOnStartCharge && lastTimeShot + delayBetweenShots < Time.time)
        {
            UseAmmo(ammoUsedOnStartCharge);
            isCharging = true;
           // playerMovement.s();

            return true;
        }

        return false;
    }

    bool TryReleaseCharge()
    {
        if (isCharging)
        {
            HandleShoot(currentCharge);
            currentCharge = 0f;
            isCharging = false;

            return true;
        }
        return false;
    }
    #endregion



    #region HandleShoot

    void HandleShoot(float _currentCharge)
    {
        if (attackPrefab != null)
        {
            if (spawnedVFX == null && mainVFXPrefab != null)
            {
                SpawnVFX(mainVFXPrefab);
            }
            Debug.Log("Shooting");
            //spawn all bullets with random direction
            for (int i = 0; i < bulletsPerShot; i++)
            {
                if (hasAuthority)
                {
                    CmdHandleShoot(_currentCharge);
                }
                Debug.Log("CmdHandleShootCall");
            }
            lastTimeShot = Time.time;

        }
        else
        {
            Debug.Log("Not Main Attack Prefab Ability");
        }
    }

    [Command]
    void CmdHandleShoot(float _currentCharge)
    {
        Debug.Log("CMDHandleShoot");
        // if(!isServer) return;
        Vector3 shotDirection = GetShotDirectionWithinSpread(playerAim.GetAimDir());
        GameObject newAttack = Instantiate(attackPrefab.gameObject, playerAim.projectileSpawnTrans.position, Quaternion.LookRotation(shotDirection));
        NetworkConnection ownerConn = owner.GetComponent<NetworkIdentity>().connectionToClient;
        NetworkServer.Spawn(newAttack, ownerConn); // ,ownerConn
        RpcHandleShoot(newAttack, _currentCharge);
        Debug.Log("Shot spawn position " + newAttack.transform.position);
    }

    [ClientRpc]
    void RpcHandleShoot(GameObject newAttack, float _currentCharge)
    {
        AttackBase attack = newAttack.GetComponent<AttackBase>();
        attack.SetCharge(_currentCharge);
        Debug.Log("RPCHandleShoot");
        attack.Shoot(this, AbilitySlot.Main);
    }


    #endregion


    #region HandleVFX



    public void SpawnVFX(GameObject _spawnedVFX)
    {
        spawnedVFX = Instantiate(_spawnedVFX, playerAim.projectileSpawnTrans.transform.position, playerAim.projectileSpawnTrans.transform.rotation);
        if (NetworkServer.active)
        {
            NetworkServer.Spawn(spawnedVFX);
            spawnedVFX.transform.parent = playerAim.projectileSpawnTrans;

        }
    }
    public void RemoveVFX()
    {
        if (spawnedVFX != null)
        {
            Destroy(spawnedVFX);
        }
    }




    #endregion
    public Vector3 GetShotDirectionWithinSpread(Vector3 _shootVector)
    {
        float spreadAngleRatio = bulletSpreadAngle / 180f;
        Vector3 spreadWorldDirection = Vector3.Slerp(_shootVector, UnityEngine.Random.insideUnitSphere * spreadMultiplier , spreadAngleRatio);

        return spreadWorldDirection;
    }

    public float GetMainAmmoRatio()
    {
        
        return currentAmmoRatio;
    }
    public float GetAltAmmoRatio()
    {
        return currentAltAmmoRatio;
    }
    public float GetSpecAmmoRatio()
    {
        return currentSpecAmmoRatio;
    }

}
