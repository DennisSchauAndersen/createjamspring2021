using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPointSingle : MonoBehaviour
{
    //public bool IsContested;
    private bool _isTaken;

    public List<MeshRenderer> CheckPointRenderer;

    private CheckPointController _controller;

    private List<PlayerTeam.Team> _teamsInCheckPoint;

    public List<PlayerTeam.Team> ContestedByTeams;

    public Material BaseMaterial;
    public Material TeamRedMaterial;
    public Material TeamBlueMaterial;


    private int _redTeamCounter;
    private int _blueTeamCounter;

    public int CountToTakeCheckPoint;

    public PlayerTeam.Team TakenByTeam;

    public event EventHandler<CheckPointTakenEventArgs> OnCheckPointTaken;
    public event EventHandler<CheckPointBeingTakenEventArgs> OnCheckPointBeingTaken;


    public void InjectCheckPointController(CheckPointController controller)
    {
        _controller = controller;
    }

    private void Awake()
    {
        _teamsInCheckPoint = new List<PlayerTeam.Team>();
        ContestedByTeams = new List<PlayerTeam.Team>();
        ResetCounters();
        ChangeRenderMaterial();
    }

    private void OnTriggerEnter(Collider other)
    {

    }

    private void OnTriggerExit(Collider other)
    {
        if (other.TryGetComponent<PlayerTeam>(out PlayerTeam playerTeam))
        {
            //_controller.PlayerInCheckPoint(this);

            if (_teamsInCheckPoint.Contains(playerTeam.CurrentTeam))
            {
                _teamsInCheckPoint.Remove(playerTeam.CurrentTeam);
            }
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.TryGetComponent<PlayerTeam>(out PlayerTeam playerTeam))
        {
            //_controller.PlayerInCheckPoint(this);

            if (!_teamsInCheckPoint.Contains(playerTeam.CurrentTeam))
            {
                _teamsInCheckPoint.Add(playerTeam.CurrentTeam);
            }

            if (ContestedByTeams.Count > 0 
                    && ContestedByTeams.Contains(playerTeam.CurrentTeam) 
                    && _teamsInCheckPoint.Count == 1)
            {


                TakeCheckPoint(playerTeam);
            }
        }
    }

    private void TakeCheckPoint(PlayerTeam team)
    {
        if (team.CurrentTeam == TakenByTeam)
        {
            return;
        }

        switch (team.CurrentTeam)
        {
            case PlayerTeam.Team.Red:
                _redTeamCounter++;
                OnCheckPointBeingTaken?.Invoke(this, 
                    new CheckPointBeingTakenEventArgs((float)_redTeamCounter / CountToTakeCheckPoint,
                        team.CurrentTeam));
                break;

            case PlayerTeam.Team.Blue:
                _blueTeamCounter++;
                OnCheckPointBeingTaken?.Invoke(this, 
                    new CheckPointBeingTakenEventArgs((float)_blueTeamCounter / CountToTakeCheckPoint,
                        team.CurrentTeam));
                break;
        }

        EvaluateCheckPointTaken();
        Debug.Log($"Trigger count: {_redTeamCounter}");
        Debug.Log($"Trigger count: {_blueTeamCounter}");
    }

    private void EvaluateCheckPointTaken()
    {

        if (_blueTeamCounter == CountToTakeCheckPoint)
        {
            TakenByTeam = TakenByTeam == PlayerTeam.Team.None ? PlayerTeam.Team.Blue : PlayerTeam.Team.None;
            _isTaken = true;
        }

        if (_redTeamCounter == CountToTakeCheckPoint)
        {
            TakenByTeam = TakenByTeam == PlayerTeam.Team.None ? PlayerTeam.Team.Red : PlayerTeam.Team.None;
            _isTaken = true;
        }

        if (_isTaken)
        {
            ResetCounters();
            ChangeRenderMaterial();

            _isTaken = false;

            OnCheckPointTaken?.Invoke(this, new CheckPointTakenEventArgs(TakenByTeam));
        }
    }

    public void ChangeRenderMaterial(){
        Material materialToChange;
        switch (TakenByTeam)
        {
            case PlayerTeam.Team.Red:
                materialToChange = TeamRedMaterial;
                break;

            case PlayerTeam.Team.Blue:
                materialToChange = TeamBlueMaterial;
                break;
            
            default:
                materialToChange = BaseMaterial;
                break;
        }

        for (int i = 0; i < CheckPointRenderer.Count; i++)
        {

            Material[] materials = CheckPointRenderer[i].materials;
            materials[1] = materialToChange;
            CheckPointRenderer[i].materials = materials;


        }
    }

    private void ResetCounters()
    {
        _blueTeamCounter = 0;
        _redTeamCounter = 0;
    }

}