using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using RootMotion.FinalIK;
public class CharacterShoot : NetworkBehaviour
{

    public NetworkGamePlayerPT netPlayer;
    public WeaponController currWeapon;
    public GameObject weapon;
    public Transform weaponTransform;
    public AimIK aimIK;
    public PlayerAim playerAim;
    public Smooth.SmoothSyncMirror smoothSyncMirror;

    public Vector3 weaponPosition;
    public Vector3 weaponRotation;
    private void Awake()
    {
        playerAim = gameObject.GetComponent<PlayerAim>();
        Invoke("CmdSpawnDefaultWeapon", 2f);
    }

    [Command]
    public void CmdSpawnDefaultWeapon()
    {
            GameObject go = Instantiate(weapon,weaponPosition,Quaternion.Euler(weaponRotation));
            NetworkConnection ownerConn = gameObject.GetComponent<NetworkIdentity>().connectionToClient;
            NetworkServer.Spawn(go, ownerConn);

        if (!isServer) return;
        RpcSpawnDefaultWeapon(go);
    }
    [ClientRpc]
    public void RpcSpawnDefaultWeapon(GameObject weapon)
    {
        currWeapon = weapon.GetComponent<WeaponController>();
        currWeapon.transform.parent = weaponTransform;
        currWeapon.transform.localPosition = weaponPosition;
        currWeapon.transform.localRotation = Quaternion.Euler(weaponRotation);
        aimIK.solver.transform = currWeapon.aimTransform;
        currWeapon.playerAim = playerAim;
        playerAim.projectileSpawnTrans = currWeapon.weaponMuzzle;
        currWeapon.playerMovement = gameObject.GetComponent<ECM.Controllers.BaseCharacterController>();
        currWeapon.owner = gameObject;
        smoothSyncMirror.childObjectToSync = currWeapon.gameObject;
    }
    void Update()
    {
        if (hasAuthority)
        {
            if (currWeapon)
            {
                if (Input.GetButtonDown("Fire1"))
                {
                    currWeapon.HandleShootInputs(true, false, false);
                }
                if (Input.GetButton("Fire1"))
                {
                    currWeapon.HandleShootInputs(false, true, false);
                }
                if (Input.GetButtonUp("Fire1"))
                {
                    currWeapon.HandleShootInputs(false, false, true);
                }
            }
        }
    }
}
