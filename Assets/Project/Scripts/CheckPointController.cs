using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPointController : MonoBehaviour
{
    public event EventHandler<LastCheckPointTakenEventArgs> OnLastCheckPointTaken;
    public event EventHandler<CheckPointTakenEventArgs> OnCheckPointTaken;
    public event EventHandler<CheckPointBeingTakenEventArgs> OnCheckPointBeingTaken;
    public bool GameOver;
    
    private List<CheckPointSingle> _checkPoints;

    private int _redTeamCheckPointIndex;
    private int _blueTeamCheckPointIndex;


    private void Awake() {
        _checkPoints = new List<CheckPointSingle>();

        for (int i = 0; i < transform.childCount; i++)
        {
            Transform checkPointTransform = transform.GetChild(i);

            CheckPointSingle checkPoint = checkPointTransform.GetComponent<CheckPointSingle>();
            checkPoint.InjectCheckPointController(this);

            checkPoint.OnCheckPointTaken 
                += (sender, args) => HandleCheckPointTaken(sender, args);
            checkPoint.OnCheckPointBeingTaken 
                += (sender, args) => HandleCheckPointBeingTaken(sender, args);

            _checkPoints.Add(checkPoint);     
        }

        _redTeamCheckPointIndex = 4;
        _blueTeamCheckPointIndex = 0;

        EvalutateContested();
    }

    private void Start() {
        GameOver = false;
    }

    public void HandleCheckPointTaken(object sender, CheckPointTakenEventArgs eventArgs)
    {
        int takenIndex = _checkPoints.FindIndex(x => (CheckPointSingle)sender == x);
        PlayerTeam.Team takenByTeam = eventArgs.TakenByTeam;

        // If the checkpoint is now taken by none, then it's the only contested checkpoint
        if (takenByTeam == PlayerTeam.Team.None)
        {
            OnCheckPointTaken?.Invoke(this, 
                new CheckPointTakenEventArgs(takenByTeam, takenIndex));

            return;
        }


        int takenIndexIncrement = takenByTeam == PlayerTeam.Team.Red ? -1 : 1;
        switch (takenByTeam)
        {
            case PlayerTeam.Team.Red:
                _redTeamCheckPointIndex += takenIndexIncrement;

                if (_redTeamCheckPointIndex == _blueTeamCheckPointIndex)
                {
                    _blueTeamCheckPointIndex += takenIndexIncrement;
                }
                break;

            case PlayerTeam.Team.Blue:
                _blueTeamCheckPointIndex += takenIndexIncrement;
                if (_redTeamCheckPointIndex == _blueTeamCheckPointIndex)
                {
                    _redTeamCheckPointIndex += takenIndexIncrement;
                }
                
                break;
            
            default:
                break;
        }

        OnCheckPointTaken?.Invoke(this, 
                new CheckPointTakenEventArgs(takenByTeam, takenIndex));

        if (takenIndex == 0 || takenIndex == _checkPoints.Count - 1)
        {
            Debug.Log("Game over!");
            GameOver = true;

            OnLastCheckPointTaken?.Invoke(this, new LastCheckPointTakenEventArgs(takenByTeam));
            return;
        }

        EvalutateContested();       
    }

    public void HandleCheckPointBeingTaken(object sender, CheckPointBeingTakenEventArgs eventArgs)
    {
        eventArgs.CheckPointIndex = _checkPoints.FindIndex(x => (CheckPointSingle)sender == x);
        OnCheckPointBeingTaken?.Invoke(this, eventArgs);
    }

    public void EvalutateContested()
    {
        // Clear points
        foreach (var checkPoint in _checkPoints)
        {
            checkPoint.ContestedByTeams.Clear();
        }
        _checkPoints[_redTeamCheckPointIndex - 1].ContestedByTeams.Add(PlayerTeam.Team.Red);
        _checkPoints[_blueTeamCheckPointIndex + 1].ContestedByTeams.Add(PlayerTeam.Team.Blue);
    }
}

public class LastCheckPointTakenEventArgs : EventArgs
{
    public PlayerTeam.Team WinningTeam;

    public LastCheckPointTakenEventArgs(PlayerTeam.Team winningTeam)
    {
        WinningTeam = winningTeam;
    }
}

public class CheckPointTakenEventArgs : EventArgs
{
    public PlayerTeam.Team TakenByTeam;
    public int CheckPointIndex;

    public CheckPointTakenEventArgs(PlayerTeam.Team takenByTeam)
    {
        TakenByTeam = takenByTeam;
    }

    public CheckPointTakenEventArgs(PlayerTeam.Team takenByTeam, int checkPointIndex){
        TakenByTeam = takenByTeam;
        CheckPointIndex = checkPointIndex;
    }
}

public class CheckPointBeingTakenEventArgs : EventArgs
{
    public float NormalisedCheckPointCount;
    public PlayerTeam.Team TakingTeam;
    public int CheckPointIndex;

    public CheckPointBeingTakenEventArgs(float normalisedCheckPointCount, 
        PlayerTeam.Team takingTeam)
    {
        NormalisedCheckPointCount = normalisedCheckPointCount;
        TakingTeam = takingTeam;
    }

    public CheckPointBeingTakenEventArgs(float normalisedCheckPointCount, 
        PlayerTeam.Team takingTeam, int checkPointIndex)
    {
        NormalisedCheckPointCount = normalisedCheckPointCount;
        TakingTeam = takingTeam;
        CheckPointIndex = checkPointIndex;
    }
}


