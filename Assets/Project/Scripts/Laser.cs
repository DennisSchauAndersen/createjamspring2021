using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Laser : MonoBehaviour
{

    private Animator laserAnimator;
    [SerializeField]
    private float shootingDistance = 2f;

    [SerializeField]
    private GameObject explosion;

    [SerializeField]
    private LineRenderer laser;

    private Vector3 rayPosition = Vector3.zero;

    private Transform transform;

    void Awake()
    {
        transform = GetComponent<Transform>();
        laserAnimator = this.GetComponent<Animator>();
    }

    void Update()
    {
        if (Input.GetKeyDown("space"))
        {
            Debug.Log("Shoot");
            ShootLaser();
        }
    }

    void ShootLaser()
    {

        laserAnimator.Play("Shoot");

        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.forward, out hit, shootingDistance, Physics.DefaultRaycastLayers))
        {
            
            rayPosition = transform.InverseTransformPoint(hit.point);
            //laser.SetPosition(1, rayPosition);
            Debug.Log(rayPosition);
        }
        //else
        //{
         //   rayPosition = transform.InverseTransformDirection(transform.forward) * shootingDistance; 
         //   laser.SetPosition(1, rayPosition);
         //   Debug.Log(rayPosition);
        //}
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        //Check that it is being run in Play Mode, so it doesn't try to draw this in Editor mode
        //Draw a cube where the OverlapBox is (positioned where your GameObject is as well as a size)
        //Gizmos.DrawWireCube(transform.position, transform.localScale);
        //Gizmos.DrawRay(this.transform.position, this.transform.forward);
        //Gizmos.DrawWireCube(rayPosition, transform.localScale);
    }
}
