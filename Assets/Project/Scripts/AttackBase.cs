﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Mirror;

public class AttackBase : NetworkBehaviour
{
    public GameObject owner { get; private set; }

    public WeaponController weapon;


    public Vector3 initialPosition { get; private set; }
    public Vector3 initialDirection { get; private set; }
    public Vector3 inheritedMuzzleVelocity { get; private set; }
    public float currentCharge { get; private set; }
    public float maxChargeDuration { get; private set; }


    public UnityAction onShoot;

    public List<AnimatorOverrideController> animatorOverrides;


    public void Shoot(WeaponController controller, AbilitySlot _abilitySlot)
    {
        owner = controller.owner;
        weapon = controller;
        initialPosition = transform.position;
        initialDirection = transform.forward;
        if (onShoot != null)
        {
            onShoot.Invoke();
        }
    }

    public void SetCharge(float _currentCharge)
    {
        currentCharge = _currentCharge;
    }
}
