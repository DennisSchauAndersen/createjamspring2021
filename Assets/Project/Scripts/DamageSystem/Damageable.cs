﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using System;
using UnityEngine;
using UnityEngine.Serialization;

using UnityEngine.UI;
using TMPro;
using Mirror;
public struct DamageMessage 
{
    public int _amount;
    public int _ragdollForce;
    public int _pushForce;
    public float _staggerDuration;
    public float _staggerAmount;
    public float _knockbackAmount;

    public Vector3 _direction;
    public Transform _damageSource;
    public bool _throwing;

    public bool stopCamera;

}

[System.Serializable]
public enum HealthType
{
    Hits,
    Health
}

public class Damageable : NetworkBehaviour
{


    public Image testHealthText = null;

    public event Action OnHitWhileInvulnerable, OnBecomeVulnerable, OnResetDamage;
    public UnityEvent hitEvent;

    /// <summary>
    /// Dispatched when health changes with old, new, and max health values.
    /// </summary>
    public event Action<int, int, int> OnHealthChanged;
    /// <summary>
    /// Dispatched when health is depleted.
    /// </summary>
    public event Action<DamageMessage> OnDeath;
    /// <summary>
    /// Dispatched after being respawned.
    /// </summary>
    public event Action OnRespawned;
    /// <summary>
    /// Current health.
    /// </summary>
    /// 
    [SyncVar]
    public int CurrentHealth;
    /// <summary>
    /// Maximum amount of health character can currently achieve.
    /// </summary>
    public int MaximumHealth { get { return _baseHealth; } }


    #region Serialized.
    /// <summary>
    /// Health to start with.
    /// </summary>
    [Tooltip("Health to start with.")]
    [SerializeField]
    private int _baseHealth = 100;
    #endregion
    public float invulnerabiltyTime;
    public float healthCooldownTime;
    public float deathTime;


    [Tooltip("The angle from the which that damageable is hitable. Always in the world XZ plane, with the forward being rotate by hitForwardRoation")]
    [Range(0.0f, 360.0f)]
    public float hitAngle = 360.0f;
    [Tooltip("Allow to rotate the world forward vector of the damageable used to define the hitAngle zone")]
    [Range(0.0f, 360.0f)]
    [FormerlySerializedAs("hitForwardRoation")] //SHAME!
    public float hitForwardRotation = 360.0f;

    public bool isInvulnerable { get; set; }
    public bool isDead { get; set; }


    protected float m_timeSinceLastHit = 0.0f;

    protected float m_timeSinceDeath = 0.0f;

    protected Collider m_Collider;

    System.Action schedule;


    public KillUI killUi;


    public HealthType healthType;
    private void Awake()
    {
    }
    void Start()
    {
        RestoreHealth();
        m_Collider = GetComponent<Collider>();
    }

    void Update()
    {
        if (isInvulnerable)
        {
            m_timeSinceLastHit += Time.deltaTime;
            if (m_timeSinceLastHit > invulnerabiltyTime)
            {
                m_timeSinceLastHit = 0.0f;
                isInvulnerable = false;
                OnBecomeVulnerable?.Invoke();
                Respawned();
            }
        }
        else if(CurrentHealth < MaximumHealth)
        {
            m_timeSinceLastHit += Time.deltaTime;
            if (m_timeSinceLastHit > healthCooldownTime)
            {
                m_timeSinceLastHit = 0.0f;
               // RestoreHealth();
            }
        }
    }


    public void ResetDamage()
    {
        RestoreHealth();
        isInvulnerable = false;
        m_timeSinceLastHit = 0.0f;
        OnResetDamage?.Invoke();

    }

    public void SetColliderState(bool enabled)
    {
        m_Collider.enabled = enabled;
    }

    public void ApplyDamage(DamageMessage msg)
    {
        Debug.Log("ApplyDamage");
        if (isServer)
        {

            RpcApplyDamage(msg);
        }
        else
        {
            if (!isLocalPlayer)
                return;
            CmdApplyDamage(msg);
        }
    }
    [Command]
    public void CmdApplyDamage(DamageMessage msg)
    {
        if (!isServer) return;
        Debug.Log("CmdApplyDamage");
        RpcApplyDamage(msg);
    }

    [ClientRpc]
    public void  RpcApplyDamage(DamageMessage data)
    {
        Debug.Log("RpcApplyDamage");

        if (CurrentHealth <= 0)
        {//ignore damage if already dead. TODO : may have to change that if we want to detect hit on death...
            return;
        }
        

        if (isInvulnerable)
        {
            OnHitWhileInvulnerable?.Invoke();
            return;
        }

        Vector3 forward = transform.forward;
        forward = Quaternion.AngleAxis(hitForwardRotation, transform.up) * forward;

        //we project the direction to damager to the plane formed by the direction of damage
        Vector3 positionToDamager = data._damageSource.position - transform.position;
        positionToDamager -= transform.up * Vector3.Dot(transform.up, positionToDamager);

        if (Vector3.Angle(forward, positionToDamager) > hitAngle * 0.5f)
            return;

        int oldHealth = 0;
        Vector3 dir = Vector3.zero;
        switch (healthType)
        {
            case HealthType.Hits:
                isInvulnerable = true;
                RemoveHealth(1,data);
                dir = transform.position - data._damageSource.position;
                break;
            case HealthType.Health:
                isInvulnerable = true;
                RemoveHealth(data._amount,data);
                dir = transform.position - data._damageSource.position;
                break;
            default:
                break;
        }



        if (CurrentHealth <= 0) 
        { 
            isDead = true;
            //schedule += OnDeath.Invoke; //This avoid race condition when objects kill each other.
        }
        else
        {
            OnHealthChanged?.Invoke(oldHealth, CurrentHealth,MaximumHealth);
        }

    }

    //void LateUpdate()
    //{
    //    if (schedule != null)
    //    {
    //        schedule();
    //        schedule = null;
    //    }
    //}


    #region HealthFunctions
    /// <summary>
    /// Received when a hitbox is hit.
    /// </summary>
    /// <param name="arg1"></param>
    /// <param name="arg2"></param>



    /// <summary>
    /// Restores health to maximum health.
    /// </summary>
    public void RestoreHealth()
    {
        int oldHealth = CurrentHealth;
        CurrentHealth = MaximumHealth;
        UpdateHealth(CurrentHealth);
        OnHealthChanged?.Invoke(oldHealth, CurrentHealth, MaximumHealth);

        if (base.isServer)
            RpcRestoreHealth();
    }

    /// <summary>
    /// Called when respawned.
    /// </summary>
    public void Respawned()
    {
        OnRespawned?.Invoke();

        if (base.isServer)
            RpcRespawned();
    }

    ///// <summary>
    ///// Removes health.
    ///// </summary>
    ///// <param name="value"></param>
    ///// <param name="multiplier"></param>
    //public void RemoveHealth(int value, float multiplier)
    //{
    //    RemoveHealth(Mathf.CeilToInt(value * multiplier));
    //}

    /// <summary>
    /// Removes health.
    /// </summary>
    /// <param name="value"></param>
    public void RemoveHealth(int value,DamageMessage dmg)
    {
        int oldHealth = CurrentHealth;
        CurrentHealth -= value;
        hitEvent.Invoke();
        UpdateHealth(CurrentHealth);
        OnHealthChanged?.Invoke(oldHealth, CurrentHealth, MaximumHealth);

        if (CurrentHealth <= 0f)
            HealthDepleted(dmg);
    }

    /// <summary>
    /// Called when health is depleted.
    /// </summary>
    public virtual void HealthDepleted(DamageMessage dmg)
    {
        Debug.Log("Health Depleted on" + gameObject.name);
        dmg._damageSource.gameObject.GetComponent<Damageable>().killUi.IncrementKill();
        killUi.IncrementDeath();
        OnDeath?.Invoke(dmg);
    }

    /// <summary>
    /// Sent to clients when health is restored.
    /// </summary>
    [ClientRpc]
    private void RpcRestoreHealth()
    {
        //Server already restored health. If we don't exit this will be an endless loop. This is for client host.
        if (base.isServer)
            return;

        RestoreHealth();
    }

    /// <summary>
    /// Sent to clients when character is respawned.
    /// </summary>
    [ClientRpc]
    private void RpcRespawned()
    {
        if (base.isServer)
            return;

        Respawned();
    }


    /// <summary>
    /// Sent to clients to remove a portion of health.
    /// </summary>
    /// <param name="value"></param>
    [ClientRpc]
    private void RpcRemoveHealth(int value, int priorHealth,DamageMessage dmg)
    {
        //Server already removed health. If we don't exit this will be an endless loop. This is for client host.
        if (base.isServer)
            return;

        /* Set current health to prior health so that
         * in case client somehow magically got out of sync
         * this will fix it before trying to remove health. */
        CurrentHealth = priorHealth;

        RemoveHealth(value,dmg);
    }
    #endregion

    void UpdateHealth(float _currentHealth)
    {
        if(testHealthText != null)
        {
            testHealthText.fillAmount = _currentHealth/MaximumHealth;
        }
    }
    



#if UNITY_EDITOR
    private void OnDrawGizmosSelected()
    {
        Vector3 forward = transform.forward;
        forward = Quaternion.AngleAxis(hitForwardRotation, transform.up) * forward;

        if (Event.current.type == EventType.Repaint)
        {
            UnityEditor.Handles.color = Color.blue;
            UnityEditor.Handles.ArrowHandleCap(0, transform.position, Quaternion.LookRotation(forward), 1.0f,
                EventType.Repaint);
        }


        UnityEditor.Handles.color = new Color(1.0f, 0.0f, 0.0f, 0.5f);
        forward = Quaternion.AngleAxis(-hitAngle * 0.5f, transform.up) * forward;
        UnityEditor.Handles.DrawSolidArc(transform.position, transform.up, forward, hitAngle, 1.0f);
    }
#endif
}



