﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using System;
using UnityEngine;
using UnityEngine.Serialization;

using Mirror;

public class PlayerDamageable : Damageable
{

    public event Action OnHitWhileInvulnerable, OnBecomeVulnerable, OnResetDamage;
    /// <summary>
    /// Dispatched when health changes with old, new, and max health values.
    /// </summary>
    public event Action<int, int, int> OnHealthChanged;
    /// <summary>
    /// Dispatched when health is depleted.
    /// </summary>
    public event Action OnDeath;
    /// <summary>
    /// Dispatched after being respawned.
    /// </summary>
    public event Action OnRespawned;
    /// <summary>
    /// Current health.
    /// </summary>
    /// 
    [SyncVar]
    public int CurrentHealth;
    /// <summary>
    /// Maximum amount of health character can currently achieve.
    /// </summary>
    public int MaximumHealth { get { return _baseHealth; } }


    #region Serialized.
    /// <summary>
    /// Health to start with.
    /// </summary>
    [Tooltip("Health to start with.")]
    [SerializeField]
    private int _baseHealth = 100;
    #endregion
    public float invulnerabiltyTime;
    public float deathTime;
    public float staggerTime;


    [Tooltip("The angle from the which that damageable is hitable. Always in the world XZ plane, with the forward being rotate by hitForwardRoation")]
    [Range(0.0f, 360.0f)]
    public float hitAngle = 360.0f;
    [Tooltip("Allow to rotate the world forward vector of the damageable used to define the hitAngle zone")]
    [Range(0.0f, 360.0f)]
    [FormerlySerializedAs("hitForwardRoation")] //SHAME!
    public float hitForwardRotation = 360.0f;

    public bool isInvulnerable { get; set; }
    public bool isDead { get; set; }


    protected float m_timeSinceLastHit = 0.0f;
    protected float m_timeSinceLastStagger = 0.0f;

    protected float m_timeSinceDeath = 0.0f;

    protected Collider m_Collider;

    System.Action schedule;
    

    public float currentHealth = 100f;
    private void Awake()
    {
    }
    void Start()
    {
        RestoreHealth();
        m_Collider = GetComponent<Collider>();
    }

    void Update()
    {
        if (isInvulnerable)
        {
            m_timeSinceLastHit += Time.deltaTime;
            if (m_timeSinceLastHit > invulnerabiltyTime)
            {
                m_timeSinceLastHit = 0.0f;
                isInvulnerable = false;
                OnBecomeVulnerable?.Invoke();
            }
        }
        //if(ragdollController != null)
        //{
        //    if (isDead)
        //    {
        //        m_timeSinceDeath += Time.deltaTime;
        //        if (m_timeSinceDeath > deathTime)
        //        {
        //            m_timeSinceDeath = 0.0f;
        //            isDead = false;
        //            if (isServer)
        //            {
        //                Debug.Log("Ragdoll RPC");
        //                ragdollController.RpcToggleRagdoll(Vector3.zero, 0f);
        //            }
        //            else
        //            {
        //                if (!isLocalPlayer)
        //                    return;
        //                Debug.Log("Ragdoll CMD");
        //                ragdollController.CmdRagdoll(Vector3.zero, 0f);
        //            }
        //            RestoreHealth();

        //        }
        //    }
        //    if (ragdollController.playerMovement.slowdownAmount != 1f)
        //    {
        //        m_timeSinceLastStagger += Time.deltaTime;
        //        if (m_timeSinceLastStagger > staggerTime)
        //        {
        //            m_timeSinceLastStagger = 0.0f;
        //            ragdollController.playerMovement.slowdownAmount = 1f;

        //        }
        //    }
        //}
    }
    #region KnockbackFunctions

    public void Knockback(float knockbackAmount, Vector3 knockbackDir)
    {

        //if (isServer)
        //{
        //    Debug.Log("Knockback RPC");
        //    RpcKnockback(knockbackAmount, knockbackDir);
        //}
        //else
        //{
        //    if (!isLocalPlayer)
        //        return;
        //    Debug.Log("Knockback CMD");
        //    CmdKnockback(knockbackAmount, knockbackDir);

        //}
    }


    #endregion
    #region StaggerFunctions

    public void Stagger(float staggerAmount, float staggerDuration)
    {

        if (isServer)
        {
            Debug.Log("stagger RPC");
            RpcStagger(staggerAmount, staggerDuration);
        }
        else
        {
            if (!isLocalPlayer)
                return;
            Debug.Log("Stagger CMD");
            CmdStagger(staggerAmount, staggerDuration);
            
        }
    }
    [ClientRpc]
    public void RpcStagger(float staggerAmount, float staggerDuration)
    {
        staggerTime = staggerDuration;
       // ragdollController.playerMovement.slowdownAmount = staggerAmount;
    }
    [Command]
    public void CmdStagger(float staggerAmount, float staggerDuration)
    {
        if (!isServer) return;

        RpcStagger(staggerAmount, staggerDuration);
    }

    #endregion

    public void ResetDamage()
    {
        RestoreHealth();
        isInvulnerable = false;
        m_timeSinceLastHit = 0.0f;
        OnResetDamage?.Invoke();

    }

    public void SetColliderState(bool enabled)
    {
        m_Collider.enabled = enabled;
    }

    public void ApplyDamage(DamageMessage msg)
    {
        Debug.Log("Hit Player");
      //  if(ragdollController == null) return;
        if (isServer)
        {
            Debug.Log("Ragdoll RPC");
            RpcApplyDamage(msg);
        }
        else
        {
            if (!isLocalPlayer)
                return;
            Debug.Log("Ragdoll CMD");
            CmdApplyDamage(msg);
        }
    }
    [Command]
    public void CmdApplyDamage(DamageMessage msg)
    {
        if (!isServer) return;

        RpcApplyDamage(msg);
    }

    [ClientRpc]
    public void  RpcApplyDamage(DamageMessage data)
    {

        if (CurrentHealth <= 0)
        {//ignore damage if already dead. TODO : may have to change that if we want to detect hit on death...
            return;
        }
        

        if (isInvulnerable)
        {
            OnHitWhileInvulnerable?.Invoke();
            return;
        }

        Vector3 forward = transform.forward;
        forward = Quaternion.AngleAxis(hitForwardRotation, transform.up) * forward;

        //we project the direction to damager to the plane formed by the direction of damage
        Vector3 positionToDamager = data._damageSource.position - transform.position;
        positionToDamager -= transform.up * Vector3.Dot(transform.up, positionToDamager);

        if (Vector3.Angle(forward, positionToDamager) > hitAngle * 0.5f)
            return;
        Stagger(data._staggerAmount, data._staggerDuration);
        isInvulnerable = true;
        int oldHealth = CurrentHealth;
        CurrentHealth -= data._amount;
        Vector3 dir = transform.position - data._damageSource.position;
        Knockback(data._knockbackAmount, dir.normalized);
     //   ragdollController.playerMovement.HurtFromDirection(dir.normalized.x, dir.normalized.y);

        if (CurrentHealth <= 0) 
        { 
         //   ragdollController.RpcToggleRagdoll(dir.normalized, data._ragdollForce);
            isDead = true;
            //schedule += OnDeath.Invoke; //This avoid race condition when objects kill each other.
        }
        else
        {
            OnHealthChanged?.Invoke(oldHealth, CurrentHealth,MaximumHealth);
        }

    }
    public void PushPlayer(DamageMessage msg)
    {
        if (currentHealth <= 0)
        {//ignore damage if already dead. TODO : may have to change that if we want to detect hit on death...
            return;
        }
        

        // if (isInvulnerable)
        // {
        //     OnHitWhileInvulnerable?.Invoke();
        //     return;
        // }
        currentHealth -= msg._amount;
        Debug.Log(currentHealth + gameObject.name);
        if(currentHealth <= 0)
        {

           // ragdollController.mainRb.velocity = Vector3.up * msg._knockbackAmount;    

            // PushPlayer1(Vector3.up * msg._knockbackAmount);
            currentHealth = 100f;
        }
    }

    //void PushPlayer1(Vector3 dirAndForce)
    //{
    //    if(isServer)
    //    {
    //        RpcPushPlayer1(dirAndForce);
    //    }
    //    else
    //    {
    //      //  ragdollController.mainRb.velocity = dirAndForce;    
    //        CmdPushPlayer1(dirAndForce);
    //    }
    //}

    //[Command]
    //void CmdPushPlayer1(Vector3 dirAndForce)
    //{
    //    ragdollController.mainRb.velocity = dirAndForce;
    //}
    //[ClientRpc]
    //void RpcPushPlayer1(Vector3 dirAndForce)
    //{
    //    ragdollController.mainRb.velocity = dirAndForce;
    //}


    //void LateUpdate()
    //{
    //    if (schedule != null)
    //    {
    //        schedule();
    //        schedule = null;
    //    }
    //}


    #region HealthFunctions
    /// <summary>
    /// Received when a hitbox is hit.
    /// </summary>
    /// <param name="arg1"></param>
    /// <param name="arg2"></param>



    /// <summary>
    /// Restores health to maximum health.
    /// </summary>
    public void RestoreHealth()
    {
        int oldHealth = CurrentHealth;
        CurrentHealth = MaximumHealth;

        OnHealthChanged?.Invoke(oldHealth, CurrentHealth, MaximumHealth);

        if (base.isServer)
            RpcRestoreHealth();
    }

    /// <summary>
    /// Called when respawned.
    /// </summary>
    public void Respawned()
    {
        OnRespawned?.Invoke();

        if (base.isServer)
            RpcRespawned();
    }

    /// <summary>
    /// Removes health.
    /// </summary>
    /// <param name="value"></param>
    /// <param name="multiplier"></param>
    public void RemoveHealth(int value, float multiplier)
    {
        RemoveHealth(Mathf.CeilToInt(value * multiplier));
    }

    /// <summary>
    /// Removes health.
    /// </summary>
    /// <param name="value"></param>
    public void RemoveHealth(int value)
    {
        int oldHealth = CurrentHealth;
        CurrentHealth -= value;

        OnHealthChanged?.Invoke(oldHealth, CurrentHealth, MaximumHealth);

        if (CurrentHealth <= 0f)
            HealthDepleted();

        if (base.isServer)
            RpcRemoveHealth(value, oldHealth);
    }

    /// <summary>
    /// Called when health is depleted.
    /// </summary>
    public virtual void HealthDepleted()
    {
        OnDeath?.Invoke();
    }

    /// <summary>
    /// Sent to clients when health is restored.
    /// </summary>
    [ClientRpc]
    private void RpcRestoreHealth()
    {
        //Server already restored health. If we don't exit this will be an endless loop. This is for client host.
        if (base.isServer)
            return;

        RestoreHealth();
    }

    /// <summary>
    /// Sent to clients when character is respawned.
    /// </summary>
    [ClientRpc]
    private void RpcRespawned()
    {
        if (base.isServer)
            return;

        Respawned();
    }


    /// <summary>
    /// Sent to clients to remove a portion of health.
    /// </summary>
    /// <param name="value"></param>
    [ClientRpc]
    private void RpcRemoveHealth(int value, int priorHealth)
    {
        //Server already removed health. If we don't exit this will be an endless loop. This is for client host.
        if (base.isServer)
            return;

        /* Set current health to prior health so that
         * in case client somehow magically got out of sync
         * this will fix it before trying to remove health. */
        CurrentHealth = priorHealth;

        RemoveHealth(value);
    }
    #endregion

    



#if UNITY_EDITOR
    private void OnDrawGizmosSelected()
    {
        Vector3 forward = transform.forward;
        forward = Quaternion.AngleAxis(hitForwardRotation, transform.up) * forward;

        if (Event.current.type == EventType.Repaint)
        {
            UnityEditor.Handles.color = Color.blue;
            UnityEditor.Handles.ArrowHandleCap(0, transform.position, Quaternion.LookRotation(forward), 1.0f,
                EventType.Repaint);
        }


        UnityEditor.Handles.color = new Color(1.0f, 0.0f, 0.0f, 0.5f);
        forward = Quaternion.AngleAxis(-hitAngle * 0.5f, transform.up) * forward;
        UnityEditor.Handles.DrawSolidArc(transform.position, transform.up, forward, hitAngle, 1.0f);
    }
#endif
}



