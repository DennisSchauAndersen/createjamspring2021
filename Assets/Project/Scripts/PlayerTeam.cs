using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using UnityEngine.UI;
public class PlayerTeam : NetworkBehaviour
{
    public NetworkGamePlayerPT playerPT;
    public TeamContainer TeamContainer;
    public Image healthImage;

  //  public GameObject alienArm;
    public GameObject alienHead;
    public enum Team
    {
        Red,
        Blue,
        None
    }
    [SyncVar]
    public Team CurrentTeam;
    public void Start()
    {
        SetPlayerTeam();

        Invoke("FindAndAddTeamContainer", 3);
    }


    public void SetPlayerTeam()
    {
        if (playerPT.playerNumber % 2 == 0)
        {
            Debug.Log("Set Team to blue");
            CurrentTeam = PlayerTeam.Team.Blue;
            healthImage.color = Color.blue;
 
        }
        else
        {
            Debug.Log("Set Team to red");
            CurrentTeam = PlayerTeam.Team.Red;
            healthImage.color = Color.red;
        }
        //Debug.Log("Team Set");

        //if (isServer)
        //{
        //    Debug.Log("Try RPC");
        //    RpcSetPlayerTeam();
        //}
        //else
        //{
        //    Debug.Log("Try Cmd");

        //    if (!isLocalPlayer)
        //        return;

        //    CmdSetPlayerTeam();
        //}
    }
    [Command]
    public void CmdSetPlayerTeam()
    {
        if (!isServer) return;
        Debug.Log("SetPlayerTeamCMD");
        RpcSetPlayerTeam();
    }
    [ClientRpc]
    public void RpcSetPlayerTeam()
    {
        Debug.Log("SetPlayerTeamRPC");

        if (playerPT.playerNumber % 2 == 0)
        {
            Debug.Log("Set Team to red");
            CurrentTeam = PlayerTeam.Team.Red;
            healthImage.color = Color.red;
        }
        else
        {
            Debug.Log("Set Team to blue");
            CurrentTeam = PlayerTeam.Team.Blue;
            healthImage.color = Color.blue;

        }
    }

    public void HandlePlayerAdded()
    {
        var playersInPlay = TeamContainer.GetPlayersInPlay();

        foreach (var player in playersInPlay)
        {
            var playerTeam = player.GetComponent<PlayerTeam>();

            switch (playerTeam.CurrentTeam)
            {   
                case Team.Blue:  
                    // Blue as human
                    // Red as alien
                    foreach (var otherPlayer in playersInPlay)
                    {
                        var otherPlayerPlayerTeam = otherPlayer.GetComponent<PlayerTeam>();
                        if (otherPlayerPlayerTeam.CurrentTeam == PlayerTeam.Team.Red)
                        {
                           // otherPlayerPlayerTeam.alienArm.SetActive(true);
                            otherPlayerPlayerTeam.alienHead.SetActive(true);
                        }
                    }
                    break;

                case Team.Red:
                    // Red as human
                    // Blue as alien
                    foreach (var otherPlayer in playersInPlay)
                    {
                        var otherPlayerPlayerTeam = otherPlayer.GetComponent<PlayerTeam>();
                        if (otherPlayerPlayerTeam.CurrentTeam == PlayerTeam.Team.Blue)
                        {
                          //  otherPlayerPlayerTeam.alienArm.SetActive(true);
                            otherPlayerPlayerTeam.alienHead.SetActive(true);
                        }
                    }
                    break;

                default:
                    break;
            }
        
        }
    }

    public void FindAndAddTeamContainer()
    {
        TeamContainer = GameObject.FindGameObjectWithTag("TeamContainer").GetComponent<TeamContainer>();
        // Add Player to container
        TeamContainer.AddPlayer(gameObject);
        HandlePlayerAdded();
    }
}
