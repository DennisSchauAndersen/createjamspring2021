using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;



public class ProjectileBase : NetworkBehaviour
{
    [Header("General")]
    [SerializeField] private Rigidbody rb;
    [SerializeField] private Collider col;
    [SerializeField] private GameObject visuals;
    [Tooltip("Radius of this projectile's collision detection")]
    public float radius = 0.01f;
    [Tooltip("Transform representing the root of the projectile (used for accurate collision detection)")]
    public Transform root;
    [Tooltip("Transform representing the tip of the projectile (used for accurate collision detection)")]
    public Transform tip;
    [Tooltip("LifeTime of the projectile")]
    public float maxLifeTime = 5f;
    [Tooltip("VFX prefab to spawn upon impact")]
    public GameObject impactVFX;
    [Tooltip("LifeTime of the VFX before being destroyed")]
    public float impactVFXLifetime = 5f;
    [Tooltip("Offset along the hit normal where the VFX will be spawned")]
    public float impactVFXSpawnOffset = 0.1f;
    [Tooltip("Clip to play on impact")]
    public AudioClip impactSFXClip;
    [Tooltip("Layers this projectile can collide with")]
    public LayerMask hittableLayers = -1;
    [Tooltip("Layer of the player")]
    public LayerMask playerLayer = -1;

    [Header("Movement")]
    [Tooltip("Speed of the projectile")]
    public float speed = 20f;
    [Tooltip("Y offset of shooting direction")]
    public float yOffsetOfAim = 0f;
    [Tooltip("Min time before gravity sets in")]
    public float minGravityTime = 0.3f;
    [Tooltip("Max time before gravity sets in")]
    public float maxGravityTime = 0.3f;
    [Tooltip("Downward acceleration from gravity")]
    public float gravityDownAcceleration = 0f;
    [Tooltip("Distance over which the projectile will correct its course to fit the intended trajectory (used to drift projectiles towards center of screen in First Person view). At values under 0, there is no correction")]
    public float trajectoryCorrectionDistance = -1;
    [Tooltip("Determines if the projectile inherits the velocity that the weapon's muzzle had when firing")]
    public bool inheritWeaponVelocity = false;

    [Header("Damage")]
    [Tooltip("Damage of the projectile")]
    public float damage = 40f;
    public float knockbackAmount;
    public float staggerAmount;
    public float staggerDuration;
    public int ragdollForce;
    //[Tooltip("Area of damage. Keep empty if you don<t want area damage")]
    //public DamageArea areaOfDamage;
    [Header("Debug")]
    [Tooltip("Color of the projectile radius debug view")]
    public Color radiusColor = Color.cyan * 0.2f;

    AttackBase attackBase;
    Vector3 lastRootPosition;
    Vector3 velocity;
    bool hasTrajectoryOverride;
    float shootTime;
    Vector3 trajectoryCorrectionVector;
    Vector3 consumedTrajectoryCorrectionVector;
    List<Collider> ignoredColliders;
    Vector3 lastFrameVelo = Vector3.zero;
    public bool hasHit = false;
    public bool hasBeenFired = false;
    float gravityTime;

    void OnEnable()
    {
        attackBase = GetComponent<AttackBase>();

        attackBase.onShoot += OnShoot;
      //  Destroy(gameObject, maxLifeTime);
    }

    void FixedUpdate()
    {
        if (hasHit) return;
        if (lastFrameVelo == Vector3.zero)
        {
            visuals.transform.forward = rb.velocity;
        }
        else
        {
            visuals.transform.forward = lastFrameVelo;
        }
        lastFrameVelo = rb.velocity;

    }

    void OnShoot()
    {
        if (isServer)
        {
            RpcOnShoot();
        }
        else 
        {
            if (!isLocalPlayer)
                return;
            CmdOnShoot();
        }

    }

    [ClientRpc]
    void RpcOnShoot()
    {
        shootTime = Time.time;
        //lastRootPosition = root.position;
        // Vector3 dir = attackBase.initialDirection.normalized;
        Vector3 dir = attackBase.owner.GetComponent<PlayerAim>().GetAimDir();
        velocity = dir * speed;
        //gravityTime = PTHelpers.ReMap(attackBase.currentCharge, 0, 1, minGravityTime, maxGravityTime);
        //StartCoroutine(SetGravity());
        //Debug.Log("OnShoot called, Dir = " + dir);
        //velocity =  AimCalcs.CalculateVelocityHitTarget(AimCalcs.GetAimHitScan(attackBase.weapon.weaponMuzzle.position, attackBase.owner.GetComponent<PlayerAim>().GetCamDirRay().direction, 50f, true), attackBase.weapon.weaponMuzzle.position, 1f);
        //Debug.DrawRay(transform.position, dir * 50f, Color.cyan);
        ignoredColliders = new List<Collider>();

        Collider[] ownerColliders = attackBase.owner.GetComponentsInChildren<Collider>();
        ignoredColliders.AddRange(ownerColliders);
        ignoredColliders.Add(attackBase.weapon.GetComponentInChildren<Collider>());
        foreach (var iCol in ignoredColliders)
        {
            Physics.IgnoreCollision(col, iCol);
        }

        rb.velocity = velocity;
        hasBeenFired = true;
    }
    [Command]
    void CmdOnShoot()
    {
        RpcOnShoot();
    }

    IEnumerator SetGravity()
    {
        rb.useGravity = false;
        yield return new WaitForSeconds(gravityTime);
        rb.useGravity = true;
    }

    bool IsHitValid(Collider _col)
    {
        if (ignoredColliders.Contains(_col))
        {
            return false;
        }
        return true;
    }

    void OnCollisionEnter(Collision other)
    {
        if (!hasBeenFired) return;
        if (PTHelpers.CompareLayers(playerLayer,other.gameObject.layer))
        {
            if (other.gameObject.TryGetComponent(out Damageable damageable) && !hasHit)
            {
                var msg = new DamageMessage()
                {
                    _ragdollForce = ragdollForce,
                    _staggerDuration = staggerDuration,
                    _staggerAmount = staggerAmount,
                    _knockbackAmount = knockbackAmount,
                    _amount = (int)damage,
                    _direction = other.contacts[0].normal,
                    _damageSource = attackBase.owner.gameObject.transform,
                };
                Debug.Log("Apply damage to" + msg._amount + damageable.gameObject);

                damageable.ApplyDamage(msg);
                hasHit = true;
                rb.isKinematic = true;
                rb.velocity = Vector3.zero;
                visuals.transform.forward = lastFrameVelo;
                gameObject.transform.parent = other.gameObject.transform;
                col.enabled = false;
                gameObject.SetActive(false);
                return;
            }
        }
        else if(PTHelpers.CompareLayers(hittableLayers, other.gameObject.layer))
        {
            hasHit = true;
            rb.isKinematic = true;
            rb.velocity = Vector3.zero;
            visuals.SetActive(false);
          
            col.enabled = false;
            return;
        }
    }



    //[Command]
    //void CmdSpawnBalloon(Vector3 pos, GameObject newParent)
    //{

    //    if (!isServer) return;

    //    RpcSpawnBalloon( pos, newParent);

    //}

    //[ClientRpc]
    //void RpcSpawnBalloon( Vector3 pos, GameObject newParent)
    //{
    //    GameObject Balloon = Instantiate(BalloonPrefab, pos, Quaternion.identity);
    //    NetworkServer.Spawn(Balloon);
    //    attackBase.weapon.LimitObjects(Balloon, attackBase.abilitySlot);
    //    //Balloon.GetComponent<Toys_Balloon>().anchorRigidbody.transform.parent = newParent;
    //    //Balloon.GetComponent<Toys_Balloon>().anchorTrans.position = pos;
    //    Balloon.GetComponent<Toys_Balloon>().AnchorBalloon(newParent, pos);
    //}

    // void OnCollisionStay(Collision other) 
    // {
    //     if(!ignoredColliders.Contains(other.collider))
    //     {
    //         if(IsHitValid(other.collider))
    //         {
    //             Debug.Log("Hit " + other.gameObject.GetComponent<NetworkGamePlayerPT>().GetDisplayName());
    //             ignoredColliders.Add(other.collider);

    //         }
    //     }    
    // }
}


