﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PTHelpers
{
    /// <summary>
    /// Compare a Layermask to a layer.
    /// </summary>
    /// <param name="layerMask"> The layermask to compare against.</param>
    /// <param name="layer"> The layer that should be compared to the layermask</param>
    /// <returns> Return a bool regarding if the layer is within the Layermask</returns>
    static public bool CompareLayers(LayerMask layerMask, int layer)
    {
        if(layerMask == (layerMask | 1 << layer))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /// <summary>
    /// Remap a float value from one range to another.
    /// </summary>
    /// <param name="value"> The float value to remap</param>
    /// <param name="from1"> The original range minimum parameter</param>
    /// <param name="to1"> The original range maximum parameter</param>
    /// <param name="from2"> The desired range minimum parameter</param>
    /// <param name="to2"> The desired range maximum parameter</param>
    /// <returns> Returns a float value that is within the range of from2-to2</returns>

    static public float ReMap(float value, float from1, float to1, float from2, float to2)
    {
        return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
    }


}
