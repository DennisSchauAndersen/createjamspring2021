﻿using Mirror;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using TMPro;
using System.Linq;
using UnityEngine.SceneManagement;
using Smooth;
public class NetworkGamePlayerPT : NetworkBehaviour
{


    //[SyncVar(hook = nameof(SetIngameDisplayName))]
    //private string displayName = "Loading...";

    //[SerializeField]
    //private bool keyboardIsControllerType;
    ////[SerializeField] TMP_Text ingameDisplayNameText = null;

    //[SerializeField]
    //public List<Material> playerMaterials;
    //[SerializeField]
    //public SkinnedMeshRenderer playerMesh;
    [SyncVar]
    public int playerNumber;
    public List<GameObject> spawnPositions;
    public Camera cam;
    public Damageable damageable;
    private NetworkManagerPT room;
    private NetworkManagerPT Room
    {
        get
        {
            if (room != null) { return room; }
            return room = NetworkManager.singleton as NetworkManagerPT;
        }
    }

    void OnEnable()
    {
        Debug.Log("OnEnable called");
        SceneManager.sceneLoaded += OnSceneLoaded;
        damageable.OnDeath += respawn;

    }

    void OnDisable()
    {
        Debug.Log("OnEnable called");
        SceneManager.sceneLoaded -= OnSceneLoaded;
        damageable.OnDeath -= respawn;

    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        Debug.Log("OnSceneLoaded: " + scene.name);
        spawnPositions.Clear();
        spawnPositions.AddRange(GameObject.FindGameObjectsWithTag("SpawnPosition"));
        if (spawnPositions.Count > 0)
        {
            spawnPositions = spawnPositions.OrderBy(go => float.Parse(go.name)).ToList();

            //spawnPositions = spawnPositions.OrderBy(go => go.name).ToList();
            gameObject.transform.position = spawnPositions[playerNumber].transform.position;
        }

        if (isLocalPlayer)
        {
            cam.gameObject.SetActive(true);
            cam.transform.parent = null;
            DontDestroyOnLoad(cam);
        }
    }

    public void respawn(DamageMessage damageMessage)
    {
        damageable.ResetDamage();
        transform.position = spawnPositions[playerNumber].transform.position;
    }
    public override void OnStartClient()
    {
        DontDestroyOnLoad(gameObject);

        Room.GamePlayers.Add(this);



    }

    public override void OnStartAuthority()
    {

    }
    
    //public override void OnNetworkDestroy()
    //{
    //    Room.GamePlayers.Remove(this);
    //}


    [Server]
    public void SetDisplayName(string displayName)
    {
      //  this.displayName = displayName;
        //SetIngameDisplayName();
    }

    //public string GetDisplayName()
    //{
    // //   return this.displayName;
    //}

    //public void SetControllerType(bool value)
    //{
    //   // this.keyboardIsControllerType = value;
    //}

    //public bool GetControllerType()
    //{
    //   // return this.keyboardIsControllerType;
    //}

    //public bool PlayerIsUsingKeyboard()
    //{
    // //   return this.keyboardIsControllerType;
    //}


    //public void SetIngameDisplayName(string oldValue, string newValue)
    //{
    //   // ingameDisplayNameText.text = displayName;
    //}

    //[Command]
    //void CmdSetDisplayName(string displayName)
    //{
    //   // this.displayName = displayName;
    //}

//    [Command]
//    void CmdSetControllerType(bool value)
//    {
//   7/     this.keyboardIsControllerType = value;
//    }
}
