﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;


public class JoinLobbyMenu : MonoBehaviour
{
    [SerializeField] private NetworkManagerPT networkManager = null;

    [Header("UI")]
    [SerializeField] private GameObject landingPagePanel = null;
    [SerializeField] private TMP_InputField ipAddressInputField = null;
    [SerializeField] private TMP_InputField portInputField = null;
    [SerializeField] private TMP_Text portErrorText = null;
    [SerializeField] private Button joinButton = null;

    private void OnEnable() 
    {
        NetworkManagerPT.OnClientConnected += HandleClientConnected;
        NetworkManagerPT.OnClientDisconnected += HandleClientDisconnected;
        if(ipAddressInputField.text == null)
        {
            ipAddressInputField.text = "localhost";
        }
        portErrorText.enabled = false;

    }
    private void OnDisable() 
    {
        NetworkManagerPT.OnClientConnected -= HandleClientConnected;
        NetworkManagerPT.OnClientDisconnected -= HandleClientDisconnected;

    }

    public void JoinLobby()
    {
        
        string ipAddress = ipAddressInputField.text;
        //string port = portInputField.text;
        //try
        //{
        //    networkManager. = int.Parse(port);
        //}
        //catch (System.Exception)
        //{
        //    ThrowPortError();
        //    Debug.Log("port only accepts numerical values");
        //    return;
        //}
        
        //networkManager.networkPort = int.Parse(port);
        networkManager.networkAddress = ipAddress;
        networkManager.StartClient();

        joinButton.interactable = false;
    }

    public void HandleClientConnected()
    {
        joinButton.interactable = true;

        gameObject.SetActive(false);
        landingPagePanel.SetActive(false);
        
    }

    public void HandleClientDisconnected()
    {
        joinButton.interactable = true;
    }

    void ThrowPortError()
    {
        StartCoroutine(PortErrorMessege());
    }

    IEnumerator PortErrorMessege()
    {
        portErrorText.enabled = true;
        yield return new WaitForSeconds(2f);
        portErrorText.enabled = false;
    }

}