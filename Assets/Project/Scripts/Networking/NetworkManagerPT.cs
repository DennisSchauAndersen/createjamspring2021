﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using System.Linq;
using Mirror;
using System;
using UnityEngine.SceneManagement;
using System.Linq;

public class NetworkManagerPT : NetworkManager
{

    ////public ControllerManager controllerManager;
    //[SerializeField] private int minPlayers = 1;
    //[Scene] [SerializeField] private string menuScene = string.Empty;

    //[Header("Room")]
    //[SerializeField] private NetworkRoomPlayerPT roomPlayerPrefab = null;

    //[Header("Game")]
    //[SerializeField] private NetworkGamePlayerPT gamePlayerPrefab = null;

    //public static event Action OnClientConnected;
    //public static event Action OnClientDisconnected;

    //public List<NetworkRoomPlayerPT> RoomPlayers { get; } = new List<NetworkRoomPlayerPT>();
    //public List<NetworkGamePlayerPT> GamePlayers = new List<NetworkGamePlayerPT>();

    //public GameObject defaultWeapon;

    //public string sceneToLoad;

    //public override void OnStartServer()
    //{

    //    spawnPrefabs = Resources.LoadAll<GameObject>("SpawnablePrefabs").ToList();

    //    StartGame();
    //}

    //public override void OnStartClient()
    //{
    //    var spawnablePrefabs = Resources.LoadAll<GameObject>("SpawnablePrefabs");

    //    foreach (var prefab in spawnablePrefabs)
    //    {
    //        ClientScene.RegisterPrefab(prefab);
    //    }
    //}

    //public override void OnClientConnect(NetworkConnection conn)
    //{
    //    base.OnClientConnect(conn);


    //    OnClientConnected?.Invoke();
    //}

    //public override void OnClientDisconnect(NetworkConnection conn)
    //{
    //    base.OnClientDisconnect(conn);

    //    OnClientDisconnected?.Invoke();
    //}

    //public override void OnServerConnect(NetworkConnection conn)
    //{
    //    if (numPlayers >= maxConnections)
    //    {
    //        conn.Disconnect();
    //        return;
    //    }

    //    // if(SceneManager.GetActiveScene().path != menuScene)
    //    // {
    //    //     conn.Disconnect();
    //    //     return;
    //    // }

    //}

    //public override void OnServerAddPlayer(NetworkConnection conn)
    //{
    //    if (SceneManager.GetActiveScene().path == menuScene)
    //    {
    //        bool isLeader = RoomPlayers.Count == 0;

    //        NetworkRoomPlayerPT roomPlayerInstance = Instantiate(roomPlayerPrefab);


    //        roomPlayerInstance.IsLeader = isLeader;

    //        NetworkServer.AddPlayerForConnection(conn, roomPlayerInstance.gameObject);

    //        //UpdateAmountOfConnections();
    //    }
    //    else if (SceneManager.GetActiveScene().path != menuScene)
    //    {
    //        // NetworkRoomPlayerPT roomPlayerInstance = Instantiate(roomPlayerPrefab);

    //        // roomPlayerInstance.IsLeader = false;

    //        // NetworkServer.AddPlayerForConnection(conn, roomPlayerInstance.gameObject);
    //        float spawnOffset = 0f;

    //        Vector3 randSpawnPos = new Vector3(UnityEngine.Random.Range(-2, 2) + spawnOffset, 10, UnityEngine.Random.Range(-2, 2) + spawnOffset);
    //        var gameplayerInstance = Instantiate(gamePlayerPrefab, transform.position, Quaternion.identity);
    //        //gameplayerInstance.SetDisplayName(gamePlayerInstance.DisplayName);
    //        //gameplayerInstance.SetControllerType(gamePlayerInstance.GetControllerType());
    //        NetworkServer.Spawn(gameplayerInstance.gameObject, conn);
    //        //NetworkServer.Destroy(conn.identity.gameObject); //Destroys roomplayer
    //        NetworkServer.AddPlayerForConnection(conn, gameplayerInstance.gameObject); //replaces their conn.identity to the gameplayer
    //        //controllerManager.AssignNextPlayer(gameplayerInstance);
    //    }
    //}

    //public override void OnServerDisconnect(NetworkConnection conn)
    //{
    //    if (conn.identity != null)
    //    {
    //        var player = conn.identity.GetComponent<NetworkRoomPlayerPT>();

    //        RoomPlayers.Remove(player);

    //        NotifyPlayersOfReadyState();

    //    }

    //    base.OnServerDisconnect(conn);
    //}

    //public override void OnStopServer()
    //{
    //    RoomPlayers.Clear();
    //}

    //public void NotifyPlayersOfReadyState()
    //{
    //    foreach (var player in RoomPlayers)
    //    {
    //        player.HandleReadyToStart(IsReadyToStart());
    //        player.UpdateAmountOfConnections(RoomPlayers.Count);
    //    }
    //}

    //public void UpdateAmountOfConnections()
    //{
    //    foreach (var player in RoomPlayers)
    //    {
    //        player.UpdateAmountOfConnections(RoomPlayers.Count);
    //    }
    //}

    //private bool IsReadyToStart()
    //{
    //    if (numPlayers < minPlayers) { return false; }

    //    // foreach (var player in RoomPlayers)
    //    // {
    //    //     if(!player.IsReady) { return false; }
    //    // }

    //    return true;
    //}

    //public void StartGame()
    //{
    //    if (SceneManager.GetActiveScene().path == menuScene)
    //    {
    //        if (!IsReadyToStart()) { return; }
    //        // ServerChangeScene("Main_Hub");

    //        ServerChangeScene(sceneToLoad, false);
    //    }
    //}


    //public override void ServerChangeScene(string newSceneName, bool reloadScene)
    //{

    //    ServerChangeLogic(newSceneName, reloadScene);
    //    base.ServerChangeScene(newSceneName, reloadScene);
    //}

    //public override void OnClientSceneChanged(NetworkConnection conn)
    //{
    //    int amountOfPlayers = RoomPlayers.Count;
    //    //ControllerManager.controllerAssign(amountOfPlayers); //delegate in ControllerManager.cs

    //    base.OnClientSceneChanged(conn);

    //}

    ////[Button]
    //public void SceneChange(string newSceneName, bool reloadScene)
    //{
    //    ServerChangeScene(newSceneName, reloadScene);
    //}

    //void ServerChangeLogic(string newSceneName, bool reloadScene)
    //{
    //    if (!reloadScene)
    //    {


    //        // From menu to game
    //        if (SceneManager.GetActiveScene().path == menuScene && newSceneName.StartsWith("Main"))
    //        {
    //            Debug.Log("Replaceing roomplayers with gameplayers");
    //            int amountOfPlayers = RoomPlayers.Count;
    //            for (int i = RoomPlayers.Count - 1; i >= 0; i--)
    //            {
    //                float spawnOffset = 60f;
    //                Vector3 randSpawnPos = new Vector3(UnityEngine.Random.Range(-10, 10) + spawnOffset, 10, UnityEngine.Random.Range(-10, 10) + spawnOffset);
    //                var conn = RoomPlayers[i].connectionToClient;
    //                var gameplayerInstance = Instantiate(gamePlayerPrefab, randSpawnPos, Quaternion.identity);
    //                //   gameplayerInstance.SetDisplayName(RoomPlayers[i].DisplayName);
    //                //   gameplayerInstance.SetControllerType(RoomPlayers[i].GetControllerType());


    //                NetworkServer.Destroy(conn.identity.gameObject); //Destroys roomplayer

    //                NetworkServer.ReplacePlayerForConnection(conn, gameplayerInstance.gameObject, true); //replaces their conn.identity to the gameplayer

    //            }
    //            //GetComponent<NetworkRPC>().RpcUpdatePlayerList(amountOfPlayers);
    //            //ControllerManager.controllerAssign(amountOfPlayers); //delegate in ControllerManager.cs
    //        }
    //        else if (newSceneName.StartsWith("Main"))
    //        {

    //            Debug.Log("Replaceing roomplayers with gameplayers");
    //            int amountOfPlayers = GamePlayers.Count;
    //            for (int i = GamePlayers.Count - 1; i >= 0; i--)
    //            {
    //                float spawnOffset = 60f;
    //                // Vector3 randSpawnPos = new Vector3(UnityEngine.Random.Range(-10, 10) + spawnOffset, 10, UnityEngine.Random.Range(-10, 10) + spawnOffset);

    //                var conn = GamePlayers[i].connectionToClient;
    //                var gameplayerInstance = Instantiate(gamePlayerPrefab, Vector3.zero, Quaternion.identity);
    //                //  gameplayerInstance.SetDisplayName(GamePlayers[i].GetDisplayName());
    //                //  gameplayerInstance.SetControllerType(GamePlayers[i].GetControllerType());

    //                NetworkServer.Destroy(conn.identity.gameObject); //Destroys previous

    //                NetworkServer.ReplacePlayerForConnection(conn, gameplayerInstance.gameObject, true); //replaces their conn.identity to the gameplayer

    //            }

    //        }
    //    }
    //}

    //public string GetMenuScene()
    //{
    //    return menuScene;
    //}

    //public void AddGamePlayer(NetworkConnection conn, string _displayName, bool usingKeyboard)
    //{
    //    //CmdAddGamePlayer(NetworkConnection conn, string _displayName, bool usingKeyboard);


    //    Vector3 randSpawnPos = new Vector3(UnityEngine.Random.Range(-10, 10), 0, UnityEngine.Random.Range(-10, 10));
    //    var gameplayerInstance = Instantiate(gamePlayerPrefab, randSpawnPos, Quaternion.identity);
    //    gameplayerInstance.SetDisplayName(_displayName);
    //    //  gameplayerInstance.SetControllerType(usingKeyboard);

    //    NetworkServer.Destroy(conn.identity.gameObject); //Destroys roomplayer

    //    NetworkServer.ReplacePlayerForConnection(conn, gameplayerInstance.gameObject); //replaces their conn.identity to the gameplayer
    //}




    //////////////
    ///



    //public ControllerManager controllerManager;
    [SerializeField] private int minPlayers = 1;
    [Scene] [SerializeField] private string menuScene = string.Empty;

    [Header("Room")]
    [SerializeField] private NetworkRoomPlayerPT roomPlayerPrefab = null;

    [Header("Game")]
    [SerializeField] private NetworkGamePlayerPT gamePlayerPrefab = null;

    public static event Action OnClientConnected;
    public static event Action OnClientDisconnected;

    public List<NetworkRoomPlayerPT> RoomPlayers { get; } = new List<NetworkRoomPlayerPT>();
    public List<NetworkGamePlayerPT> GamePlayers = new List<NetworkGamePlayerPT>();

    public GameObject defaultWeapon;

    public string sceneToLoad;
    List<GameObject> currentLevelSpawnPoints = new List<GameObject>();
    int currentSpawnCount = 0;
    public override void OnStartServer()
    {

        spawnPrefabs = Resources.LoadAll<GameObject>("SpawnablePrefabs").ToList();

        StartGame();
    }

    public override void OnStartClient()
    {
        var spawnablePrefabs = Resources.LoadAll<GameObject>("SpawnablePrefabs");

        foreach (var prefab in spawnablePrefabs)
        {
            ClientScene.RegisterPrefab(prefab);
        }
    }

    public override void OnClientConnect(NetworkConnection conn)
    {
        base.OnClientConnect(conn);


        OnClientConnected?.Invoke();
    }

    public override void OnClientDisconnect(NetworkConnection conn)
    {
        base.OnClientDisconnect(conn);

        OnClientDisconnected?.Invoke();
    }

    public override void OnServerConnect(NetworkConnection conn)
    {
        if (numPlayers >= maxConnections)
        {
            conn.Disconnect();
            return;
        }

        // if(SceneManager.GetActiveScene().path != menuScene)
        // {
        //     conn.Disconnect();
        //     return;
        // }

    }

    public override void OnServerAddPlayer(NetworkConnection conn)
    {
        if (SceneManager.GetActiveScene().path == menuScene)
        {
            bool isLeader = RoomPlayers.Count == 0;

            NetworkRoomPlayerPT roomPlayerInstance = Instantiate(roomPlayerPrefab);


            roomPlayerInstance.IsLeader = isLeader;

            NetworkServer.AddPlayerForConnection(conn, roomPlayerInstance.gameObject);

            //UpdateAmountOfConnections();
        }
        else if (SceneManager.GetActiveScene().path != menuScene)
        {
            // NetworkRoomPlayerPT roomPlayerInstance = Instantiate(roomPlayerPrefab);

            // roomPlayerInstance.IsLeader = false;

            // NetworkServer.AddPlayerForConnection(conn, roomPlayerInstance.gameObject);
            float spawnOffset = 0f;

            Vector3 randSpawnPos = new Vector3(UnityEngine.Random.Range(-2, 2) + spawnOffset, 10, UnityEngine.Random.Range(-2, 2) + spawnOffset);
            var gameplayerInstance = Instantiate(gamePlayerPrefab, transform.position, Quaternion.identity);
            //gameplayerInstance.SetDisplayName(gamePlayerInstance.DisplayName);
            //gameplayerInstance.SetControllerType(gamePlayerInstance.GetControllerType());
            NetworkServer.Spawn(gameplayerInstance.gameObject, conn);
            //NetworkServer.Destroy(conn.identity.gameObject); //Destroys roomplayer
            NetworkServer.AddPlayerForConnection(conn, gameplayerInstance.gameObject); //replaces their conn.identity to the gameplayer
            //controllerManager.AssignNextPlayer(gameplayerInstance);
        }
    }

    public override void OnServerDisconnect(NetworkConnection conn)
    {
        if (conn.identity != null)
        {
            var player = conn.identity.GetComponent<NetworkRoomPlayerPT>();

            RoomPlayers.Remove(player);

            NotifyPlayersOfReadyState();

        }

        base.OnServerDisconnect(conn);
    }

    public override void OnStopServer()
    {
        RoomPlayers.Clear();
    }

    public void NotifyPlayersOfReadyState()
    {
        foreach (var player in RoomPlayers)
        {
            player.HandleReadyToStart(IsReadyToStart());
            player.UpdateAmountOfConnections(RoomPlayers.Count);
        }
    }

    public void UpdateAmountOfConnections()
    {
        foreach (var player in RoomPlayers)
        {
            player.UpdateAmountOfConnections(RoomPlayers.Count);
        }
    }

    private bool IsReadyToStart()
    {
        if (numPlayers < minPlayers) { return false; }

        // foreach (var player in RoomPlayers)
        // {
        //     if(!player.IsReady) { return false; }
        // }

        return true;
    }

    public void StartGame()
    {
        if (SceneManager.GetActiveScene().path == menuScene)
        {
            if (!IsReadyToStart()) { return; }
            // ServerChangeScene("Main_Hub");

            ServerChangeScene(sceneToLoad,false);
        }
    }


    public override void ServerChangeScene(string newSceneName,bool reloadScene)
    {

        ServerChangeLogic(newSceneName,reloadScene);
        base.ServerChangeScene(newSceneName,reloadScene);
    }

    public override void OnClientSceneChanged(NetworkConnection conn)
    {
        int amountOfPlayers = RoomPlayers.Count;
        //ControllerManager.controllerAssign(amountOfPlayers); //delegate in ControllerManager.cs

        base.OnClientSceneChanged(conn);

    }

    //[Button]
    public void SceneChange(string newSceneName,bool reloadScene)
    {
        ServerChangeScene(newSceneName,reloadScene);
    }

    void ServerChangeLogic(string newSceneName,bool reloadScene)
    {
        if (!reloadScene)
        {


        // From menu to game
        if (SceneManager.GetActiveScene().path == menuScene && newSceneName.StartsWith("Main"))
        {

            int amountOfPlayers = RoomPlayers.Count;
            for (int i = RoomPlayers.Count - 1; i >= 0; i--)
            {
                var conn = RoomPlayers[i].connectionToClient;

                var gameplayerInstance = Instantiate(gamePlayerPrefab, Vector3.zero, Quaternion.identity);
                    //   gameplayerInstance.SetDisplayName(RoomPlayers[i].DisplayName);
                    //   gameplayerInstance.SetControllerType(RoomPlayers[i].GetControllerType());

                    gameplayerInstance.playerNumber = currentSpawnCount;
                    currentSpawnCount++;
                NetworkServer.Destroy(conn.identity.gameObject); //Destroys roomplayer
                NetworkServer.ReplacePlayerForConnection(conn, gameplayerInstance.gameObject, true); //replaces their conn.identity to the gameplayer

            }
            //GetComponent<NetworkRPC>().RpcUpdatePlayerList(amountOfPlayers);
            //ControllerManager.controllerAssign(amountOfPlayers); //delegate in ControllerManager.cs
        }
        else if (newSceneName.StartsWith("Main"))
        {

            Debug.Log("Replaceing roomplayers with gameplayers");
            int amountOfPlayers = GamePlayers.Count;
            for (int i = GamePlayers.Count - 1; i >= 0; i--)
            {
                float spawnOffset = 60f;
                    Vector3 spawnPoint;

                    // Vector3 randSpawnPos = new Vector3(UnityEngine.Random.Range(-10, 10) + spawnOffset, 10, UnityEngine.Random.Range(-10, 10) + spawnOffset);
                    if (GetSpawnPosition() != null)
                    {
                        Debug.Log("Choosing StartLocation SpawnPoints");
                        spawnPoint = GetSpawnPosition().position;
                    }
                    else
                    {
                        spawnPoint = Vector3.zero;
                    }
                    var conn = GamePlayers[i].connectionToClient;
                    var gameplayerInstance = Instantiate(gamePlayerPrefab, spawnPoint, Quaternion.identity);
                    //  gameplayerInstance.SetDisplayName(GamePlayers[i].GetDisplayName());
                    gameplayerInstance.playerNumber = i;
      
                    //  gameplayerInstance.SetControllerType(GamePlayers[i].GetControllerType());

                    NetworkServer.Destroy(conn.identity.gameObject); //Destroys previous

                NetworkServer.ReplacePlayerForConnection(conn, gameplayerInstance.gameObject, true); //replaces their conn.identity to the gameplayer

            }

        }
        }
    }
    Transform GetSpawnPosition()
    {
        currentLevelSpawnPoints.Clear();
        currentLevelSpawnPoints = GameObject.FindGameObjectsWithTag("SpawnPosition").ToList();
        if (currentLevelSpawnPoints.Count == 0)
        {
            return null;
        }
        if (currentSpawnCount < currentLevelSpawnPoints.Count)
        {
            return currentLevelSpawnPoints[currentSpawnCount++].transform;
        }
        else
        {
            currentSpawnCount = 0;
            return currentLevelSpawnPoints[currentSpawnCount++].transform;

        }
    }
    public string GetMenuScene()
    {
        return menuScene;
    }

    public void AddGamePlayer(NetworkConnection conn, string _displayName, bool usingKeyboard)
    {
        //CmdAddGamePlayer(NetworkConnection conn, string _displayName, bool usingKeyboard);


       // Vector3 randSpawnPos = new Vector3(UnityEngine.Random.Range(-10, 10), 0, UnityEngine.Random.Range(-10, 10));
        var gameplayerInstance = Instantiate(gamePlayerPrefab, Vector3.zero, Quaternion.identity);
        gameplayerInstance.SetDisplayName(_displayName);
      //  gameplayerInstance.SetControllerType(usingKeyboard);

        NetworkServer.Destroy(conn.identity.gameObject); //Destroys roomplayer

        NetworkServer.ReplacePlayerForConnection(conn, gameplayerInstance.gameObject); //replaces their conn.identity to the gameplayer
    }






}
