﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class MainMenu : MonoBehaviour
{
    [SerializeField] private NetworkManagerPT networkManager = null;

    [Header("UI")]
    [SerializeField] private GameObject landingPagePanel = null;

    public TextMeshProUGUI ip;
    public void HostLobby()
    {
        networkManager.StartHost();
        Debug.Log("Our Network Address " + networkManager.networkAddress);
        ip.text = networkManager.networkAddress;
        landingPagePanel.SetActive(false);
    }

    public void QuitApplication()
    {
        Application.Quit();
    }
}
