using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeamContainer : MonoBehaviour
{
    public List<GameObject> _playersInPlay;
    public event EventHandler PlayerAdded;

    private void Awake() {
        _playersInPlay = new List<GameObject>();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public List<GameObject> GetPlayersInPlay()
    {
        return _playersInPlay;
    }

    public void AddPlayer(GameObject playerGameObject)
    {
        _playersInPlay.Add(playerGameObject);

        PlayerAdded?.Invoke(this, EventArgs.Empty);
    }
}
