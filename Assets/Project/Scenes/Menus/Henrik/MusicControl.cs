using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;


public class MusicControl : MonoBehaviour
{

    private bool battleMusicPlaying;
    public AudioMixer MusicMixer;
    public AudioMixerSnapshot[] snapshots;
    public AudioSource[] music;
    public float[] weights;

    private AudioSource battleMusic;
    private AudioSource idleMusic;


    

    // Start is called before the first frame update
    void Start()
    {
        idleMusic = music[0];
        battleMusic = music[1];
        idleMusic.Play();
       
        BlendSnapshot(0);
        battleMusicPlaying = false;
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void BlendSnapshot(int musicState)
    {
        if(musicState == 1)
        {
            // Battle Music Starts
            if (!battleMusicPlaying)
            {
                Debug.Log("Batlle Music Plays");
                battleMusic.Play();
                battleMusicPlaying = true;
            }

            weights[0] = 0f;
            weights[1] = 1;
            MusicMixer.TransitionToSnapshots(snapshots, weights, 2.0f);
        }
        else
        {
            // Battle Music Starts
            weights[0] = 1f;
            weights[1] = 0f;
            MusicMixer.TransitionToSnapshots(snapshots, weights, 2.0f);
        }

    }
}
