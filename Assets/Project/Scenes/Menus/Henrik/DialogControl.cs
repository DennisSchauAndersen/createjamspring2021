using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class DialogControl : MonoBehaviour
{



    //public AudioMixer MusicMixer;
    //public AudioSource[] music;
    public AudioSource Intro;
    public AudioSource Sick;
    public AudioMixer DialogMixer;




    // Start is called before the first frame update
    void Start()
    {
        Intro = gameObject.AddComponent<AudioSource>();
        Sick = gameObject.AddComponent<AudioSource>();

        Intro.Play();
    }

    // Update is called once per frame
    void Update()
    {
      
    }



    public void sickAnnounce()
    {
        Sick.Play();
    }
}
