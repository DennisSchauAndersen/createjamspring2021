using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleMusicTest: MonoBehaviour
{
    // Start is called before the first frame update
    
    public MusicControl musicControl;
    
    void Start()
    {
        musicControl = GameObject.FindGameObjectWithTag("MusicControl").GetComponent<MusicControl>();


    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter(Collider col)
    {
        if(col.gameObject.tag =="Player")
        {       
            musicControl.BlendSnapshot(1);
        }
    }
    void OnTriggerExit(Collider leave)
    {
        if (leave.gameObject.tag == "Player")
        {
            Debug.Log("Player has left the battle");
            musicControl.BlendSnapshot(0);
        }
    }
}
