using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestShootingSound : Mirror.NetworkBehaviour
{
    public AudioSync audiosync;

    // Start is called before the first frame update
    void Start()
    {
        audiosync = audiosync.GetComponent<AudioSync>();
        
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
           //audiosync.PlaySound(0);
        }
    }
    void OnTriggerExit(Collider leave)
    {
        if (leave.gameObject.tag == "Player")
        {
            //audiosync.PlaySound(1);
        }
    }
}
