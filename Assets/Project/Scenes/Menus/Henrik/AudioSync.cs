using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using UnityEngine.Audio;

[RequireComponent(typeof(AudioSource))]
public class AudioSync : NetworkBehaviour
{

    private AudioSource source;
    public AudioMixer EffectMixer;
    public AudioSource[] clips;
    public WeaponController weaponController;

    // Start is called before the first frame update
    void Start()
    {
        source = this.GetComponent<AudioSource>();
        Invoke("InitiateGunSound", 2.0f);

    }

    private void InitiateGunSound()
    {
        Debug.Log("I instantiate the gun Audio");
        weaponController = GameObject.FindGameObjectWithTag("Gun").GetComponent<WeaponController>();
    }


    public void PlaySound(int id, Vector3 pos)
    {
        if (id >= 0 && id < clips.Length)
        {

            CmdSendServerSoundID(id, pos);
            Debug.Log("I try and send to the server?");
        }
        else
        {
            Debug.Log("Soundfile ID is out of bounds");
        }
    }

    [Command(ignoreAuthority = true)]
    void CmdSendServerSoundID(int id, Vector3 pos)
    {
        debugFunction("I am at the server");
        RpcSendSoundIDToClients(id, pos);
    }

    [ClientRpc]
    void RpcSendSoundIDToClients(int id, Vector3 pos)
    {
        debugFunction("I try and send it back from server");
        weaponController.shootSound();
    }

    public void debugFunction(string msg)
    {
        Debug.Log(msg);
    }

}
