using System;

public class HealthSystem
{
    public event EventHandler OnEntityDead;
    private int _currentHealth;
    private int _maxHealth;

    public void SetUp(int maxHealth)
    {
        _maxHealth = maxHealth;
        _currentHealth = _maxHealth;
    }

    public int GetCurrentHealth()
    {
        return _currentHealth;
    }

    public void TakeDamage(int damageAmount)
    {
        _currentHealth -= IsAlive() ? damageAmount : 0;
        
        // Check if entity is still alive (Queue portal music)
        IsAlive();
    }

    public void TakeHeal(int healAmount)
    {
        _currentHealth += _currentHealth < _maxHealth ? healAmount : 0;       
    }

    public bool IsAlive()
    {
        if (_currentHealth > 0)
        {
            return true;
        }

        OnEntityDead?.Invoke(this, EventArgs.Empty);
        return false;
    }
}
