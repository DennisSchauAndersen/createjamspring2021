using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestHealth : MonoBehaviour
{
    private HealthSystem _healthSystem;

    // Start is called before the first frame update
    void Start()
    {
        _healthSystem = new HealthSystem();
        _healthSystem.SetUp(100);

        _healthSystem.OnEntityDead += (x, y) => Debug.Log("Entity died!");
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("1"))
        {
            _healthSystem.TakeDamage(10);
            Debug.Log($"Health: {_healthSystem.GetCurrentHealth()}");
        }

        if (Input.GetKeyDown("2"))
        {
            _healthSystem.TakeHeal(10);
            Debug.Log($"Health: {_healthSystem.GetCurrentHealth()}");
        }
    }
}
