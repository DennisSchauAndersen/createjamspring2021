using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CheckpointUI : MonoBehaviour
{

    [SerializeField]
    private GameObject checkpointLineObj;

    [SerializeField]
    private GameObject checkpointCheckedObj;

    [SerializeField]
    private GameObject checkpointsObj;

    [SerializeField]
    private GameObject checkpointLettersObj;

    [SerializeField]
    private GameObject checkpointNextObj;

    [SerializeField]
    private Color untakenColor;

    [SerializeField]
    private Color PlayerTeamColor;

    [SerializeField]
    private Color OpponentTeamColor;

    private Image[] checkpointLines;
    private Image[] checkpointChecked;
    private RectTransform[] checkpointCheckedRect;
    private Image[] checkpoints;
    private Image[] checkpointLetters;
    private Image[] checkpointNext;

    private int blueCurrent = 0;
    private int redCurrent = 4;

    private CheckPointController controller;

    void Awake()
    {
        checkpointLines = checkpointLineObj.GetComponentsInChildren<Image>();
        checkpointChecked = checkpointCheckedObj.GetComponentsInChildren<Image>();
        checkpointCheckedRect = checkpointCheckedObj.GetComponentsInChildren<RectTransform>();
        checkpoints = checkpointsObj.GetComponentsInChildren<Image>();
        checkpointLetters = checkpointLettersObj.GetComponentsInChildren<Image>();
        checkpointNext = checkpointNextObj.GetComponentsInChildren<Image>();

        UpdateLines();
        UpdateCheckpointNext();
        UpdateCheckpointColor();
        UpdateCheckpointChecked();
        controller = GameObject.Find("CheckPoints").GetComponent<CheckPointController>();
        controller.OnCheckPointTaken += (sender, args) => HandleCheckpoint(sender, args);
        controller.OnCheckPointBeingTaken += (sender, args) => HandleCheckpointBeingTaken(sender, args);
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //if (Input.GetKeyDown("space")) {
        //    SetCheckpointPlayer(blueCurrent+1);
        //}
        //if (Input.GetKeyDown("enter")) {
        //    SetCheckpointOpponent(redCurrent-1);
        //}
        //checkpointCheckedRect[0].localEulerAngles = new Vector3(checkpointCheckedRect[0].localEulerAngles.x, checkpointCheckedRect[0].localEulerAngles.y, checkpointCheckedRect[0].localEulerAngles.z + rotSpeed);
    }

    public void HandleCheckpoint(object obj, CheckPointTakenEventArgs args) {
        int checkpoint = args.CheckPointIndex; // int?
        PlayerTeam.Team team = args.TakenByTeam; // enum
        
        if (team == PlayerTeam.Team.Blue) {
            SetCheckpointBlue(checkpoint);
        } else if (team == PlayerTeam.Team.Red) {
            SetCheckpointRed(checkpoint);
        } else if (team == PlayerTeam.Team.None) {
            // team none 3..
            if (checkpoint == blueCurrent) {
                SetCheckpointBlue(checkpoint-1);
            }
            if (checkpoint == redCurrent) {
                SetCheckpointRed(checkpoint+1);
            }
        }

    }

    public void HandleCheckpointBeingTaken(object obj, CheckPointBeingTakenEventArgs args) {
        int checkpoint = args.CheckPointIndex; // int?
        PlayerTeam.Team team = args.TakingTeam; // enum
        float progress = args.NormalisedCheckPointCount;
        var i = checkpoint+1;
        
        if (team == PlayerTeam.Team.Blue) {
            checkpointChecked[checkpoint].enabled = true;
            checkpointCheckedRect[i].Rotate(Vector3.forward, Time.deltaTime + 5f, Space.World);
            //checkpointCheckedRect[i].eulerAngles = new Vector3(checkpointCheckedRect[i].eulerAngles.x, checkpointCheckedRect[i].eulerAngles.y, checkpointCheckedRect[i].eulerAngles.z + rotSpeed);
            //Debug.Log(checkpointCheckedRect[i].localEulerAngles.z);
        } else if (team == PlayerTeam.Team.Red) {
            checkpointChecked[checkpoint].enabled = true;
            checkpointCheckedRect[i].Rotate(Vector3.forward, Time.deltaTime + 5f, Space.World);
        }
        
    }

    public void SetCheckpointBlue(int currentCheckpoint) {
        Debug.Log(currentCheckpoint);
        blueCurrent = currentCheckpoint;
        UpdateLines();
        UpdateCheckpointNext();
        UpdateCheckpointColor();
        UpdateCheckpointChecked();
    }

    public void SetCheckpointRed(int currentCheckpoint) {
        redCurrent = currentCheckpoint;
        UpdateLines();
        UpdateCheckpointNext();
        UpdateCheckpointColor();
        UpdateCheckpointChecked();
    }

    private void UpdateLines() {
        if (blueCurrent == -1) {
            return;
        }

        for(int i = 0; i < checkpointLines.Length; i++) {
            if (i <= blueCurrent) {
                checkpointLines[i].color = PlayerTeamColor;
            } else if (i >= redCurrent-1) {
                checkpointLines[i].color = OpponentTeamColor;
            } else {
                checkpointLines[i].color = untakenColor;
            }
        }
    }

    private void UpdateCheckpointChecked() {
        if (blueCurrent == -1) {
            return;
        }

        for(int i = 0; i < checkpointChecked.Length; i++) {
            if (i == blueCurrent) {
                checkpointChecked[i].enabled = true;
            } else if (i >= redCurrent-1) {
                checkpointChecked[i].enabled = true;
            } else {
                checkpointChecked[i].enabled = false;
            }
        }
    }



    private void UpdateCheckpointColor() {
        if (blueCurrent == -1) {
            return;
        }
        
        for(int i = 0; i < checkpoints.Length; i++) {
            if (i <= blueCurrent) {
                checkpoints[i].color = PlayerTeamColor;
            } else if (i >= redCurrent) {
                checkpoints[i].color = OpponentTeamColor;
            } else {
                checkpoints[i].color = untakenColor;
            }
        }   
    }

    private void UpdateCheckpointNext() {
        if (blueCurrent == -1) {
            return;
        }
        
        if (blueCurrent == checkpointNext.Length) {
            return;
        }
        if (redCurrent == -1) {
            return;
        }

        Debug.Log(blueCurrent);

        for(int i = 0; i < checkpointNext.Length; i++) {
                checkpointNext[i].enabled = false;
        }

        for(int i = 0; i < checkpointNext.Length; i++) {
            if (i == blueCurrent+1) {
                checkpointNext[i].enabled = true;
            }
            if (i == redCurrent-1) {
                checkpointNext[i].enabled = true;
            }
        }
    }
}
