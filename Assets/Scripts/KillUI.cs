using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KillUI : MonoBehaviour
{

    [SerializeField]
    private Animator killAnimator;

    [SerializeField]
    private Animator deathAnimator;

    [SerializeField]
    private Text deathText;

    [SerializeField]
    private Text killText;

    private int kills = 0;

    private int deaths = 0;

    public JuiceManager juiceManager;

    [SerializeField]
    private float incrementationDelay = 0.05f;

    void OnEnable() {
        kills = 0;
        deaths = 0;
    }

    // Start is called before the first frame update
    void Start()
    {
      //  juiceManager = GameObject.Find("JuicePanel").GetComponent<JuiceManager>();
    }

    // Update is called once per frame
    void Update()
    {
        //if (Input.GetKeyDown("space")) {
        //    IncrementKill();
        //}

    }

    public void IncrementKill() {
        kills++;
        killText.text = kills.ToString("00");
        killAnimator.Play("Increase");
        if (kills % 4 == 3) {
            juiceManager.ShowSickUI();
        }
    }

    public void IncrementDeath() {
        deaths++;
        deathText.text = deaths.ToString("00");
        deathAnimator.Play("Increase");
    }

    public void ResetKill() {
        kills = 0;
        killText.text = kills.ToString("00");
    }

    public void ResetDeath() {
        deaths = 0;
        deathText.text = kills.ToString("00");
    }
}
