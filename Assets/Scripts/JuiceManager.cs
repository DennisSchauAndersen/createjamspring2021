using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JuiceManager : MonoBehaviour
{
    public DialogControl dialogControl;
    [SerializeField]
    private Animator sickModeAnimator;

    // Start is called before the first frame update
    void Start()
    {
      //  dialogControl = GameObject.FindGameObjectWithTag("DialogControl").GetComponent<DialogControl>();
    }

    // Update is called once per frame
    void Update()
    {
        //if (Input.GetKeyDown("space")) {
        //    ShowSickUI();
        //}
    }

    public void ShowSickUI() {
        sickModeAnimator.Play("Show");
       // dialogControl.sickAnnounce();
    }
}
