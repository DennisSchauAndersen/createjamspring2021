using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
public class PlayerAim : NetworkBehaviour
{
    public Transform aimTransform;
    public LayerMask groundMask;
    public Vector3 aimOffset;
    public Transform defaultAimPosition;

    public Transform projectileSpawnTrans;
    // Start is called before the first frame update
    void Start()
    {
       // Cursor.visible = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (!isLocalPlayer) return;
   
            // If mouse right click,
            // found click position in the world

            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            RaycastHit hitInfo;
            if (!Physics.Raycast(ray, out hitInfo, Mathf.Infinity, groundMask.value))
                return;
            aimTransform.position = hitInfo.point + aimOffset;
        
    }

    public Vector3 GetAimDir()
    {
        return (aimTransform.position - projectileSpawnTrans.position).normalized;
    }
}
