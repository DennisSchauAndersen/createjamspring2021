using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using RootMotion.FinalIK;
public class SkinSwitcher : NetworkBehaviour
{
    public ECM.Controllers.BaseCharacterController characterController;
    public NetworkAnimator networkAnimator;
    public CharacterShoot characterShoot;
    public PlayerAim playerAim;

    [Header("Alien")]
    public GameObject alienMain;
    public AimIK alienAimIK;
    public LookAtIK alienLookatIK;
    public Transform alienWeaponTransform;
    public Animator alienAnimator;

    [Header("Human")]
    public GameObject humanMain;
    public AimIK humanAimIK;
    public LookAtIK humanLookatIK;
    public Transform humanWeaponTransform;
    public Animator humanAnimator;


    public void Awake()
    {
        
        SwitchVisuals(1);
    }

    [ContextMenu("SwitchVisuals")]
    public void SwitchVisuals(int i)
    {
        if (i==0)
        {
           
            characterController.charAnimator = humanAnimator;
            networkAnimator.animator = humanAnimator;
            characterShoot.aimIK = humanAimIK;
            characterShoot.weaponTransform = humanWeaponTransform;
            if (characterShoot.currWeapon != null)
            {
                characterShoot.currWeapon.transform.parent = humanWeaponTransform;
            }
            alienMain.SetActive(false);
            humanMain.SetActive(true);
        }
        else
        {

            characterController.charAnimator = alienAnimator;
            networkAnimator.animator = alienAnimator;
            characterShoot.aimIK = alienAimIK;
            characterShoot.weaponTransform = alienWeaponTransform;
            if (characterShoot.currWeapon != null)
            {
                characterShoot.currWeapon.transform.parent = alienWeaponTransform;
            }
            alienMain.SetActive(true);
            humanMain.SetActive(false);
        }
    }
}
